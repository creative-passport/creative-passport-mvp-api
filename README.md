#  Mycelia Creative Passport API

This is the Creative Passport MVP API.
It uses Twirp RPC framework. Learn more about
Twirp at its [website](https://twitchtv.github.io/twirp/docs/intro.html) or
[repo](https://github.com/twitchtv/twirp).
It also uses [go-pg](https://github.com/go-pg/pg) PostgreSQL ORM.


## Documentation

Check out `doc/doc.apib` for API documentation.
See `doc/claims.apib` for information about claims in the Creative Passport alpha and beta releases. 

## Code structure

The protobuf definitions for the services are found in the 
`rpc/` dir. There are different protobuf definitions for each of the microservices that might be called. These include services related to:
- CreativePassport creation (root identity)
- Persona creation (each of which has unique key pair/DID)
- Persona Data creation (rudimentary implementation of how data will be associated with personas)
- Service creation (for entities in the CPP ecosystem, e.g. Mycelia, Resoante, etc.)
- Service DID creation (keys and DIDs associated with root services)
- Service data creation (information associated with services/service dids)

The generated Twirp and Go protobuf code are found in the same directories.

The implementation of the server is in `internal/server`.
Database related stuff (migrations, model definitions) can be found in `internal/database`.

Finally, `cmd/server` and `cmd/client` wrap things together into executable main
packages. In this implementation, `cmd/client` is purely for testing purposes and will be eliminated on deploy. 



### Dependencies

[Dep](https://github.com/golang/dep) is used as dependency management tool.
`vendor/` folder contains project dependencies and should be in sync with `Gopkg.toml` and `Gopkg.lock`.

### Various tools installation for development

* [Install Protocol Buffers v3](https://developers.google.com/protocol-buffers/docs/gotutorial),
the `protoc` compiler that is used to auto-generate code. The simplest way to do
this is to download pre-compiled binaries for your platform from here:
https://github.com/google/protobuf/releases

It is also available in MacOS through Homebrew:

```sh
$ brew install protobuf
```

* Install [retool](https://github.com/twitchtv/retool). It helps manage go tools like commands and linters.
protoc-gen-go and protoc-gen-twirp plugins were installed into `_tools` folder using retool.

Build the generators and tool dependencies:
```sh
$ retool build
```

Then, to run the `protoc` command and autogenerate Go code for the server interface, make sure to prefix with `retool do`, for example:
```sh
$ retool do protoc --proto_path=$GOPATH/src:. --twirp_out=. --go_out=. ./rpc/creative_passport/service.proto
```

## Buildling without Docker:

### Dev database setup

* Make sure you have latest PostgreSQL installed.
* Create user and database as follow:

username = "cpp-user"

password = "password"

dbname = "cpp-dev"

Add following postgres extensions: "hstore" and "uuid-ossp"

* Run migrations from `./internal/database/migrations`

```sh
$ go run *.go
```


### Running/building the server


First, put this repo into `$GOPATH/src`

Then, run the server
```sh
$ go run ./cmd/server/main.go
```

Alternatively, you can build and run an executable binary
```sh
$ cd ./cmd/server/
$ go build
$ ./server
```

####.env file

Database configurations should be placed in `.env` file in the project root dir. An example `.env.sample` file is included to illustrate the necesssary variables. 

The `.env` file is used in `docker-compose.yaml`, in `cmd/server/main.go`, and in `config/config.go`


## Building with docker

The API and corresponding postgres database can be installed using docker-compose. This project was created with Docker 18.03.1-ce. 


### Dependencies 

At the moment, dependencies are not included in the docker-compose/docker builds. You must follow the above steps for installing protobuf (and generating protobuf files), dep, and installing the dep dependeices (`dep ensure`).

### Docker Installation

#### Dockerfiles

`Docker.cpp` is the build file for the app. `Docker.pg` is the build file for the postgres database. Both are referenced in `docker-compose.yaml`. The `build` directory contains several files called by the Dockerfiles and docker-compose in its initiation. 

These include: 
- `postgres.sql`, which is called during the Dockerfile.pg build in order to add necessary hstore and uuid-ossp exntensions to the database
- `run-testing.sh` and `run.sh`. These files control the initiation of the app. The appropriate file can be selected in `.env`. The difference is simply whether migrations are run on the testing database or the dev database. TODO: file selection should be replaced by variables and /if statements in the .sh file
- `wait-for-it.sh`: This file pauses app container initiation until after database container has been started. 


####.env file

Database configurations should be placed in `.env` file in the project root dir. An example `.env.sample` file is included to illustrate the necesssary variables. 

The `.env` file is used in `docker-compose.yaml`, in `cmd/server/main.go`, and in `config/config.go`


#### Building and Running with docker-compose

Simply run `docker-compose up` and `docker-compose down` for container management. 


#### Troubleshooting 

If your API is unable to communicate with the database, you might need to change the POSTGRES_ADDR variable in .env. The API must use the internal IP (172.Xxx.xxx) of the database container for calls. 

Do `docker ps` to get container ID and then `docker inspect <container_id>` The latter command will list the internal IP address. You can change this in .env, POSTGRES_ADDR. 


## Example curl requests

### CreateUser
```sh
curl --request "POST" \
     --location "http://localhost:8080/twirp/mycelia.api.creative_passport.CreativePassportService/CreativeCreativePassport" \
     --header "Content-Type:application/json" \
     --data '{"email": "lblisset@blisset.com", "username": "lutherblisset", "admin": false}' \
     --verbose


{"id":"9ef71770-7a1b-4a11-a81e-1b6d177a3598","username":"lutherblisset","email":"lblisset@blisset.com"}
```


## Testing

We use Ginkgo and Gomega for testing.
TODO: There is currently no docker implmeentation for testing, although the testing database can be used via steps described above. 

At the moment, you need to create the testing database and run migrations manually before running tests.

* Create user and database as follow:

username = "cpp-testing-user"

password = ""

dbname = "cpp-testing"

Add following extensions: "hstore" and "uuid-ossp" 

* Run migrations from `./internal/database/migrations`

```sh
$ go run *.go testing
```

* Run tests from `./internal/server/user` or `./internal/server/persona` (for example)

```sh
$ go test
```

Or run all tests using ginkgo CLI from `./`

```sh
$ ginkgo -r
```



### In progress (See issues in this repo)

Please note that there are several things that are incomplete or less-than-gracefully implemented at the moment. These include:

####Auth
- Auth still needs to be properly implemented. We have gone back and forth on implementations for the Alpha (with plans shifting along with timeframes), but we will try to implement a simple OIDC server for the Launch December 9. 

#### Tests
- Social Graph tests are not written yet
- Claim tests are not written yet

#### Claims
- Implement hstore claim structure in Claim struct that mirrors LifeId's claim structure

#### Services
- n.b. "Services" will not be fully implemented for the alpha; they will be created via server commands only. 
- Services need to be resticted to "admin" users
- Service data needs "get" restrictions/permissions

#### Docker
- Docker testing configuration
- Docker and database deploy config
- Docker build should include dependency installation and protobuf generation
- Run.sh file should not be switched, but rather include env variables for regulating testing vs dev env

#### Code
- Code needs to be commented
- In general, code needs to be refactored (This is my first golang project, so I was really learning in)

