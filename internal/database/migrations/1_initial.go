package main

import (

  "github.com/go-pg/migrations"
  "github.com/go-pg/pg/orm"

  "mycelia/internal/database/models"
)

func init() {
	migrations.Register(func(db migrations.DB) error {

    if _, err := db.Exec(`CREATE TYPE did_type AS ENUM ('creative_passport', 'persona', 'service');`); err != nil {
      return err
    }
    
    if _, err := db.Exec(`CREATE TYPE claim_type AS ENUM ('social', 'service','data');`); err != nil {
      return err
    }

    for _, model := range []interface{}{
      &models.StreetAddress{},
      &models.CreativePassport{},
      &models.Service{},

      &models.ServiceDid{},
      &models.Persona{},

      &models.PersonaData{},
      &models.ServiceData{},

      &models.Claim{},


    } {
      _, err := orm.CreateTable(db, model, &orm.CreateTableOptions{
        FKConstraints: true,
        IfNotExists: true,
      })
      if err != nil {
        return err
      }
    }

    if _, err := db.Exec(`alter table persona_data add foreign key (persona_id) references personas(id)`); err != nil {
      return err
    }
    if _, err := db.Exec(`alter table service_data add foreign key (service_did_id) references service_dids(id)`); err != nil {
      return err
    }

		return nil
	}, func(db migrations.DB) error {
    if _, err := db.Exec(`DROP TYPE IF EXISTS did_type CASCADE;`); err != nil {
      return err
    }
    if _, err := db.Exec(`DROP TYPE IF EXISTS claim_type CASCADE;`); err != nil {
      return err
    }

    for _, model := range []interface{}{
      &models.StreetAddress{},
      &models.CreativePassport{},
      &models.Service{},
      
      &models.ServiceDid{},
      &models.Persona{},

      &models.PersonaData{},
      &models.ServiceData{},
      
      &models.Claim{},


      } {
      _, err := orm.DropTable(db, model, &orm.DropTableOptions{
        IfExists: true,
        Cascade:  true,
      })
      if err != nil {
        return err
      }
    }

    return nil
	})
}
