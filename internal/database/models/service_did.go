package models

import (
  "time"
  // "fmt"
  // "reflect"
  "github.com/go-pg/pg"
  //"github.com/go-pg/pg/orm"
  "github.com/satori/go.uuid"
  // "github.com/twitchtv/twirp"

  // pb "mycelia/rpc/usergroup"
  //datapb "mycelia/rpc/persona_data"
  //servicepb "mycelia/rpc/service"

  //"mycelia/internal"
)

type ServiceDid struct {
  Id uuid.UUID `sql:"type:uuid,default:uuid_generate_v4()"`
  CreatedAt time.Time `sql:"default:now()"`
  UpdatedAt time.Time
  Did string `sql:",unique,notnull"`
  PublicKey string `sql:",unique,notnull"`
  PrivateKey string `sql:",unique,notnull"`
  EthereumAddress string `sql:",unique"`
  Notes string 
  Active bool `sql:",notnull"`
  DidType string  `sql:"type:did_type,notnull"`

  ServiceId uuid.UUID `sql:"type:uuid,notnull"`


  AssociatedUsers []Persona `pg:"fk:service_did_id"`

  AssociatedData []ServiceData `pg:"fk:service_did_id"`


}

func (u *ServiceDid) Delete(tx *pg.Tx) (error, string) {
  pgerr := tx.Model(u).
    Column("service_did.*", "AssociatedData").
    WherePK().
    Select()
    if pgerr != nil {
      return pgerr, "service_did"
    }

    _, pgerr = tx.Model(u).WherePK().Delete()
    if pgerr != nil {
      return pgerr, "service_did"
    }

  return nil, ""
}
