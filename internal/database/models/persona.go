package models

import (
  "time"
  // "fmt"
  // "reflect"
  "github.com/go-pg/pg"
  //"github.com/go-pg/pg/orm"
  "github.com/satori/go.uuid"
  // "github.com/twitchtv/twirp"


  //"mycelia/internal"
)

type Persona struct {
  Id uuid.UUID `sql:"type:uuid,default:uuid_generate_v4()"`

  CreatedAt time.Time `sql:"default:now()"`
  UpdatedAt time.Time
  
  Did string `sql:",unique,notnull"`
  
  PublicKey string `sql:",unique,notnull"`
  PrivateKey string `sql:",unique,notnull"`
  EthereumAddress string `sql:",unique"`
  
  Notes string 
  Active bool `sql:",notnull,default:true"`
  Privacy bool `sql:",notnull,default:false"` //true makes profile publicly visible (outside social graph)

  CreativePassportId uuid.UUID `sql:"type:uuid,notnull"`
  CreativePassport *CreativePassport

  ServiceDidId uuid.UUID `sql:"type:uuid,notnull"`
  ServiceDid *ServiceDid
  
  DidType string  `sql:"type:did_type,notnull"`

  AssociatedData []PersonaData `pg:"fk:persona_id"`


}




func (u *Persona) Delete(tx *pg.Tx) (error, string) {
  pgerr := tx.Model(u).
    Column("persona.*", "AssociatedData").
    WherePK().
    Select()
    if pgerr != nil {
      return pgerr, "persona"
    }

    for _, AssociatedData := range(u.AssociatedData) {
      pgerr, table := AssociatedData.Delete(tx)
      if pgerr != nil {
        return pgerr, table
      }
    }

    _, pgerr = tx.Model(u).WherePK().Delete()
    if pgerr != nil {
      return pgerr, "persona"
    }

  return nil, ""
}
