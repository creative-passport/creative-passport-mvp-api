package models

import (
  "time"
  //"fmt"
  // "log"
  "github.com/satori/go.uuid"
  "github.com/go-pg/pg/orm"
  //pb "mycelia/rpc/persona_data"
  "github.com/go-pg/pg"
  //"github.com/twitchtv/twirp"

  //"mycelia/internal"
)


type PersonaData struct {
  Id uuid.UUID `sql:"type:uuid,default:uuid_generate_v4()"`
  CreatedAt time.Time `sql:"default:now()"`
	UpdatedAt time.Time

  PersonaId uuid.UUID `sql:"type:uuid,notnull"`

  Biography string
  BiographyPriv bool `sql:",notnull,default:false"` //true makes visible to all (throughout)

  FullName string 
  FullNamepriv bool `sql:",notnull,default:false"`

  FirstName string `sql:",notnull"`
  FirstNamePriv bool `sql:",notnull,default:false"`

  LastName string `sql:",notnull"`
  LastNamePriv bool `sql:",notnull,default:false"`

  PhoneNumber string
  PhoneNumberPriv bool `sql:",notnull,default:false"`

  BusinessEmail string
  BusinessEmailPriv bool `sql:",notnull,default:false"`

  Avatar []byte `sql:",notnull"`
  AvatarPriv bool `sql:",notnull,default:false"`
  
  AddressId uuid.UUID  `sql:"type:uuid,notnull"`
  Address *StreetAddress
  
  AddressPriv bool `sql:",notnull,default:false"` //true makes location (city) visible  

}

func (u *PersonaData) BeforeInsert(db orm.DB) error {
  u.FullName = u.FirstName + u.LastName


  return nil
}

func (u *PersonaData) Delete(tx *pg.Tx) (error, string) {
  pgerr := tx.Model(u).
    Column("persona_data.*", "Address").
    WherePK().
    Select()
  if pgerr != nil {
    return pgerr, "persona_data"
  }

  _, pgerr = tx.Model(u).WherePK().Delete()
  if pgerr != nil {
    return pgerr, "persona_data"
  }

  _, pgerr = tx.Model(u.Address).WherePK().Delete()
  if pgerr != nil {
    return pgerr, "street_address"
  }


  return nil, ""
}
