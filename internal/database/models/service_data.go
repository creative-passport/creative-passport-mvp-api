package models

import (
  "time"
  // "fmt"
  // "log"

  //pb "mycelia/rpc/persona_data"
  "github.com/go-pg/pg"
  //"github.com/go-pg/pg/orm"

  "github.com/satori/go.uuid"
  //"github.com/twitchtv/twirp"

  //"mycelia/internal"


)


type ServiceData struct {
  Id uuid.UUID `sql:"type:uuid,default:uuid_generate_v4()"`
  CreatedAt time.Time `sql:"default:now()"`
  UpdatedAt time.Time 

  ServiceDidId uuid.UUID `sql:"type:uuid,notnull"`
  ServiceDid *ServiceDid
  
  Biography string
  Website string
  AdminContact string
  FullName string
  FirstName string
  LastName string
  PhoneNumber string
  BusinessEmail string
  Avatar []byte `sql:",notnull"`

  AddressId uuid.UUID  `sql:"type:uuid,notnull"`
  Address *StreetAddress
  
}


func (u *ServiceData) Delete(tx *pg.Tx) (error, string) {
  pgerr := tx.Model(u).
    Column("service_data.*", "Address").
    WherePK().
    Select()
  if pgerr != nil {
    return pgerr, "service_data"
  }


  _, pgerr = tx.Model(u).WherePK().Delete()
  if pgerr != nil {
    return pgerr, "service_data"
  }


  _, pgerr = tx.Model(u.Address).WherePK().Delete()
  if pgerr != nil {
    return pgerr, "street_address"
  }

  

  return nil, ""
}
