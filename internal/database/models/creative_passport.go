package models

import (
  "time"
  //"strings"
  //"os"

  "github.com/satori/go.uuid"
  //"github.com/dgrijalva/jwt-go"
  //"golang.org/x/crypto/bcrypt"

)

type CreativePassport struct {
  Id uuid.UUID `sql:"type:uuid,default:uuid_generate_v4()"`
  CreatedAt time.Time `sql:"default:now()"`
  UpdatedAt time.Time
  Email string `sql:",unique,notnull"`
  Admin bool `sql:",default:false"`
  AssociatedPersonas []Persona `pg:"fk:user_id"`
  Active bool `sql:",default:false"`
  Password []byte `sql:",notnull"`

}

