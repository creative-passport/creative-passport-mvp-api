package models

import (
  "time"
  

  "github.com/satori/go.uuid"
)

type Service struct {
  Id uuid.UUID `sql:"type:uuid,default:uuid_generate_v4()"`
  CreatedAt time.Time `sql:"default:now()"`
  UpdatedAt time.Time
  Username string `sql:",unique,notnull"`
  Email string `sql:",unique,notnull"`
  Admin bool
  OwnerOfDids []ServiceDid `pg:"fk:service_id"`


}
