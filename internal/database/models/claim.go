package models

import (

  "time"
  "github.com/satori/go.uuid"
  pb "mycelia/rpc/personadata"
  "github.com/go-pg/pg"
   "mycelia/internal"

)



type Claim struct {
  Id uuid.UUID `sql:"type:uuid,default:uuid_generate_v4()"`
  CreatedAt time.Time `sql:"default:now()"`

  Claim map[string]string `pg:",hstore"`
  Valid bool `sql:",notnull"`

  IssuingPersonaId uuid.UUID  `sql:"type:uuid,notnull"`
  
  SubjectPersonaId uuid.UUID  `sql:"type:uuid,notnull"`

  IssuingServiceId uuid.UUID  `sql:"type:uuid,notnull"`

  SubjectServiceId uuid.UUID  `sql:"type:uuid,notnull"`
  
  AssociatedServiceDidId uuid.UUID  `sql:"type:uuid,notnull"`

  DataId uuid.UUID  `sql:"type:uuid,notnull"`

  ClaimType  string  `sql:"type:claim_type,notnull"`

  SocialGraphPriv bool `sql:",default:false"` // is social graph connection public? 

}


func (c *Claim) Create(db *pg.DB, claim *pb.Claim) (error, *pb.Claim) {
    tx, err := db.Begin()
    if err != nil {
      return err, nil
    }
    defer tx.Rollback()
  
    ierr, _ := c.GetIds(claim)
      if ierr != nil {
        return ierr, nil
      }

    _, pgerr := tx.Model(c).Returning("*").Insert()
    if pgerr != nil {
      return pgerr, nil
    }


    cl, clerr := c.GetClaimResponse()
    if clerr != nil{
      return clerr, nil
    }
    return  tx.Commit(), cl

}


func (c *Claim) Update(db *pg.DB) (error, string){

  err := db.Update(c)
  if err != nil{
    return err, "claim"
  }

  return nil, ""

}


func (claim *Claim) Delete(tx *pg.Tx) (error, string) {

  pgerr := tx.Model(claim).WherePK().Select()
  if pgerr != nil {
    return pgerr, "claim"
  }


  pgerr = tx.Delete(claim)
  if pgerr != nil {
    return pgerr, "claim"
  }


  return pgerr, ""
}

func (c *Claim) GetClaimResponse() (*pb.Claim, error) {

      return &pb.Claim{
      Id: c.Id.String(),
      Valid: c.Valid,
      DataId: c.DataId.String(),
      IssuingPersonaId: c.IssuingPersonaId.String(), 
      ClaimType: c.ClaimType,

    }, nil

}


// Get ids -- this should be refactored/abstracted. 

func (c *Claim) GetIds (claim *pb.Claim) (error, string) {

    if claim.IssuingPersonaId != "" {
        issuingPersonaId, err := internal.GetUuidFromString(claim.IssuingPersonaId)
        if err != nil {
          return err, "issuing_persona_id"
        }

        c.IssuingPersonaId = issuingPersonaId

    }

    if claim.SubjectPersonaId != "" {
      subjectPersonaId, err := internal.GetUuidFromString(claim.SubjectPersonaId)
      if err != nil {
        return err, "subject_persona_id"
      }

      c.SubjectPersonaId = subjectPersonaId

    }


    if claim.IssuingServiceId != "" {
      issuingServiceId, err := internal.GetUuidFromString(claim.IssuingServiceId)
      if err != nil {
        return err, "issuing_service_id"
      }
      c.IssuingServiceId = issuingServiceId
    }


    if claim.SubjectServiceId != "" {  
     subjectServiceId, err := internal.GetUuidFromString(claim.SubjectServiceId)
      if err != nil {
        return err, "subject_service_id"
      }  
      c.SubjectServiceId = subjectServiceId

    }


    if claim.AssociatedServiceDidId != "" {
      associatedServiceDidId, err := internal.GetUuidFromString(claim.AssociatedServiceDidId)
      if err != nil {
        return err, "associated_service_did_id"
      }
      c.AssociatedServiceDidId = associatedServiceDidId

    }


    if claim.DataId != "" {
      dataId, err := internal.GetUuidFromString(claim.DataId)
      if err != nil {
        return err, "data_id"
      }
      c.DataId = dataId

    }



  return nil, ""
}


