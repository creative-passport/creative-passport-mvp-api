package personadataserver_test

import (
	// "fmt"
	// "reflect"
	"context"

	"github.com/go-pg/pg"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/personadata"
	"mycelia/internal/database/models"
)

var _ = Describe("Persona Data server", func() {
	const already_exists_code twirp.ErrorCode = "already_exists"
	const invalid_argument_code twirp.ErrorCode = "invalid_argument"
	const not_found_code twirp.ErrorCode = "not_found"

	Describe("GetPersonaData", func() {
		Context("with valid uuid", func() {
			It("should respond with personaData if it exists", func() {
				personaData := &pb.PersonaData{Id: newPersonaData.Id.String()}

				res, err := service.GetPersonaData(context.Background(), personaData)

				Expect(err).NotTo(HaveOccurred())
				Expect(res.PersonaId).To(Equal(newPersonaData.PersonaId.String()))
				Expect(res.Biography).To(Equal(newPersonaData.Biography))
				Expect(res.FullName).To(Equal(newPersonaData.FullName))
				Expect(res.FirstName).To(Equal(newPersonaData.FirstName))
				Expect(res.LastName).To(Equal(newPersonaData.LastName))
				Expect(res.PhoneNumber).To(Equal(newPersonaData.PhoneNumber))
				Expect(res.BusinessEmail).To(Equal(newPersonaData.BusinessEmail))
				Expect(res.Avatar).To(Equal(newPersonaData.Avatar))


			})
			It("should respond with not_found error if personaData does not exist", func() {
				id := uuid.NewV4()
				for id == newPersonaData.Id {
					id = uuid.NewV4()
				}
				personaData := &pb.PersonaData{Id: id.String()}
				resp, err := service.GetPersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				personaData := &pb.PersonaData{Id: id}
				resp, err := service.GetPersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("UpdatePersonaData", func() {
		Context("with valid uuid", func() {
			It("should update personaData if it exists", func() {

				personaData := &pb.PersonaData{
					Id: newPersonaData.Id.String(),
					PersonaId: newPersonaData.PersonaId.String(),
					Biography: "new bio",
					FullName: "new name",
					FirstName: newPersonaData.FirstName,
					LastName: newPersonaData.LastName,
					Avatar: newPersonaData.Avatar,
					BusinessEmail: newPersonaData.BusinessEmail,
					PhoneNumber: newPersonaData.PhoneNumber, 
					Address: &pb.StreetAddress{Id: newPersonaData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				_, err := service.UpdatePersonaData(context.Background(), personaData)

				Expect(err).NotTo(HaveOccurred())

				t := new(models.PersonaData)
				err = db.Model(t).Where("id = ?", newPersonaData.Id).Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(t.Biography).To(Equal(personaData.Biography))
				Expect(t.FirstName).To(Equal(personaData.FirstName))
				Expect(t.Avatar).To(Equal(personaData.Avatar))
				Expect(t.FullName).To(Equal(personaData.FullName))
				Expect(t.FullName).To(Equal(personaData.FullName))
				Expect(t.PersonaId.String()).To(Equal(personaData.PersonaId))
				Expect(t.BusinessEmail).To(Equal(personaData.BusinessEmail))

				addressModel := new(models.StreetAddress)
				err = db.Model(addressModel).Where("id = ?", newPersonaData.AddressId).Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(addressModel.Data).To(Equal(map[string]string{"some":"new data"}))
				// unchanged

			})
			It("should respond with not_found error if personaData does not exist", func() {
				id := uuid.NewV4()
				for id == newPersonaData.Id {
					id = uuid.NewV4()
				}
				avatar := make([]byte, 5)

				personaData := &pb.PersonaData{
					Id: id.String(),
					Biography: "best personaData ever",
					FirstName: "Amy",
					LastName: "Jones",
					Avatar: avatar,
					PersonaId: newPersonaData.PersonaId.String(),
				}
				resp, err := service.UpdatePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				//twerr := err.(twirp.Error)
				//Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				avatar := make([]byte, 5)

				personaData := &pb.PersonaData{
					Id: id,
					Biography: "best personaData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "Amy Jones",
					Avatar: avatar,
					PersonaId: newPersonaData.PersonaId.String(),
					Address: &pb.StreetAddress{Id: newPersonaData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				resp, err := service.UpdatePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("CreatePersonaData", func() {
		Context("with all required attributes", func() {
			It("should create a new personaData", func() {

				avatar := make([]byte, 5)

				personaData := &pb.PersonaData{
					Biography: "best personaData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Address: &pb.StreetAddress{Id: newPersonaData.AddressId.String(), Data: map[string]string{"some": "new data"}},
					Avatar: avatar,
					PersonaId: newPersonaData.PersonaId.String(),
				}
				resp, err := service.CreatePersonaData(context.Background(), personaData)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp).NotTo(BeNil())

				Expect(resp.Id).NotTo(BeNil())
				Expect(resp.FirstName).To(Equal(personaData.FirstName))
				Expect(resp.LastName).To(Equal(personaData.LastName))
				Expect(resp.FullName).To(Equal(personaData.FullName))
				Expect(resp.Avatar).To(Equal(personaData.Avatar))
				Expect(resp.PersonaId).To(Equal(personaData.PersonaId))

				did := new(models.Persona)
				err = db.Model(did).Where("id = ?", newPersonaData.PersonaId).Column("persona.*", "AssociatedData").Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(did.AssociatedData)).To(Equal(2))

				personaDataId, err := uuid.FromString(resp.Id)
				Expect(err).NotTo(HaveOccurred())
				var arr []uuid.UUID
				for _, data := range did.AssociatedData{
					arr = append(arr, data.Id)
				}
				Expect(arr).To(ContainElement(personaDataId))
			})
		})

		Context("with missing required attributes", func() {
			It("should not create a personaData without FirstName", func() {
				avatar := make([]byte, 5)
				personaData := &pb.PersonaData{
					Biography: "best personaData ever",
					LastName: "Jones",
					FullName: "AmyJones",
					PersonaId: newPersonaData.PersonaId.String(),
					Avatar: avatar, 
					Address:  &pb.StreetAddress{Id: newPersonaData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				resp, err := service.CreatePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("first_name"))
			})
			It("should not create a personaData without PersonaId", func() {
				avatar := make([]byte, 5)
				personaData := &pb.PersonaData{
					Biography: "best personaData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Avatar: avatar,
					Address:  &pb.StreetAddress{Id: newPersonaData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				resp, err := service.CreatePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())
				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("persona_id"))
			})
			It("should not create a personaData without avatar", func() {
				personaData := &pb.PersonaData{
					Biography: "best personaData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Address:  &pb.StreetAddress{Id: newPersonaData.AddressId.String(), Data: map[string]string{"some": "new data"}},
					PersonaId: newPersonaData.PersonaId.String(),
				}
				resp, err := service.CreatePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("avatar"))
			})
			
		})

		Context("with invalid attributes", func() {
			It("should not create a personaData if persona_id is invalid", func() {
				avatar := make([]byte, 5)
				personaData := &pb.PersonaData{
					Biography: "best personaData ever",
					FirstName: "Amy",
					LastName: "Jones",
					Avatar: avatar,
					PersonaId: "1111",
				}
				resp, err := service.CreatePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())
				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
			})
			It("should not create a personaData if PersonaId does not exist", func() {
				avatar := make([]byte, 5)

				creativePassportId := uuid.NewV4()
				for creativePassportId == newCreativePassport.Id {
					creativePassportId = uuid.NewV4()
				}
				personaData := &pb.PersonaData{
					Biography: "best personaData ever",
					FirstName: "Amy",
					LastName: "Jones",
					Avatar: avatar,
					PersonaId: creativePassportId.String(),
				}
				resp, err := service.CreatePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())
				//twerr := err.(twirp.Error)
				//Expect(twerr.Code()).To(Equal(not_found_code))
			})

		})
	})

	Describe("DeletePersonaData", func() {
		Context("with valid uuid", func() {
			It("should delete personaData if it exists", func() {
				personaData := &pb.PersonaData{Id: newPersonaData.Id.String()}

				personaDataToDelete := new(models.PersonaData)
				err := db.Model(personaDataToDelete).Where("id = ?", newPersonaData.Id).Select()
				Expect(err).NotTo(HaveOccurred())

				_, err = service.DeletePersonaData(context.Background(), personaData)

				Expect(err).NotTo(HaveOccurred())

				owner := new(models.Persona)
				err = db.Model(owner).Where("id = ?", personaDataToDelete.PersonaId).Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(owner.AssociatedData).NotTo(ContainElement(personaDataToDelete.Id))


				var personaDatas []models.PersonaData
				err = db.Model(&personaDatas).
					Where("id in (?)", pg.In([]uuid.UUID{personaDataToDelete.Id})).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(personaDatas)).To(Equal(0))
			})
			It("should respond with not_found error if personaData does not exist", func() {
				id := uuid.NewV4()
				for id == newPersonaData.Id {
					id = uuid.NewV4()
				}
				personaData := &pb.PersonaData{Id: id.String()}
				resp, err := service.DeletePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				personaData := &pb.PersonaData{Id: id}
				resp, err := service.DeletePersonaData(context.Background(), personaData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})
})
