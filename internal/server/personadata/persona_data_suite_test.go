package personadataserver_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/go-pg/pg"

	"mycelia/internal/database/models"
	"mycelia/internal/database"
	personadataserver "mycelia/internal/server/personadata"
)

var (
	db *pg.DB
	service *personadataserver.Server
	newCreativePassport *models.CreativePassport
	newPersona *models.Persona
	newPersonaData *models.PersonaData
	newService *models.Service
	newServiceDid *models.ServiceDid
	personaDataAddress *models.StreetAddress

)

func TestPersonaData(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "PersonaData server Suite")
}

var _ = BeforeSuite(func() {
	testing := true
	db = database.Connect(testing)
	service = personadataserver.NewServer(db)


	personaDataAddress := &models.StreetAddress{Data: map[string]string{"some": "data"}}
	err := db.Insert(personaDataAddress)
	Expect(err).NotTo(HaveOccurred())

	newService := &models.Service{Username: "service", Email: "service@service.io"}
	err = db.Insert(newService)
	Expect(err).NotTo(HaveOccurred())

	password := make([]byte, 5)
	newCreativePassport = &models.CreativePassport{Password: password, Email: "email@fake.com"}
	err = db.Insert(newCreativePassport)
	Expect(err).NotTo(HaveOccurred())

	newServiceDid = &models.ServiceDid{
		Did: "Did",
		PrivateKey: "PrivateKey",
		PublicKey: "PublicKey",
		DidType: "service",
		EthereumAddress: "EthAdd",
		Active: true, 
		ServiceId: newService.Id,
	}
	err = db.Insert(newServiceDid)
	Expect(err).NotTo(HaveOccurred())

	newPersona = &models.Persona{
		Did:"Did",
		PrivateKey: "PrivateKey",
		PublicKey: "PublicKey",
		EthereumAddress: "EthADdress",
		DidType: "persona",
		Active: true,
		Privacy: false, 
		ServiceDidId: newServiceDid.Id, 
		CreativePassportId: newCreativePassport.Id,
	}
	err = db.Insert(newPersona)
	Expect(err).NotTo(HaveOccurred())

	// Create a new persona_data
	avatar := make([]byte, 5)
	newPersonaData = &models.PersonaData{
		Biography: "bio",
		FirstName: "Amy",
		LastName: "Jones",
		FullName: "Amy Jones",
		Avatar: avatar,
		PersonaId: newPersona.Id,
		AddressId: personaDataAddress.Id,
	}
	err = db.Insert(newPersonaData)
	Expect(err).NotTo(HaveOccurred())

})

var _ = AfterSuite(func() {
	// delete all claims
	var claims []models.Claim
	err := db.Model(&claims).Select()
	if len(claims) > 0{
		_, err = db.Model(&claims).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	// Delete all DID data

	var personaData []models.PersonaData
	err = db.Model(&personaData).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(personaData) > 0{
		_, err = db.Model(&personaData).Delete()
		Expect(err).NotTo(HaveOccurred())	
	}

	// Delete all addresses
	var addresses []models.StreetAddress
	err = db.Model(&addresses).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(addresses) > 0{
		_, err = db.Model(&addresses).Delete()
		Expect(err).NotTo(HaveOccurred())		
	}


	// Delete all personas
	var personas []models.Persona
	err = db.Model(&personas).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(personas) > 0{
		_, err = db.Model(&personas).Delete()
		Expect(err).NotTo(HaveOccurred())	
	}


	// Delete all serviceDids
	var serviceDids []models.ServiceDid
	err = db.Model(&serviceDids).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(serviceDids) > 0{
		_, err = db.Model(&serviceDids).Delete()
		Expect(err).NotTo(HaveOccurred())
	}


	// Delete all services
	var services []models.Service
	err = db.Model(&services).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(services) > 0{
		_, err = db.Model(&services).Delete()
		Expect(err).NotTo(HaveOccurred())
	}


	// Delete all creativepassports
	var creativePassports []models.CreativePassport
	err = db.Model(&creativePassports).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(creativePassports) > 0{
		_, err = db.Model(&creativePassports).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

})

