package personadataserver

import (
	"fmt"
	"time"
	"context"

	"github.com/go-pg/pg"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/personadata"
	"mycelia/internal"
	"mycelia/internal/database/models"
)

type Server struct {
	db *pg.DB
}

func NewServer(db *pg.DB) *Server {
	return &Server{db: db}
}

func (s *Server) GetPersonaData(ctx context.Context, personaData *pb.PersonaData) (*pb.PersonaData, error) {
	id, err := internal.GetUuidFromString(personaData.Id)
	if err != nil {
		return nil, err
	}
	u := &models.PersonaData{Id: id}


	pgerr := s.db.Model(u).
			Column("persona_data.*", "Address").
			WherePK().
			Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "persona_data")
	}
	
	var cmods []models.Claim
	cmserr := s.db.Model(&cmods).Where("data_id = ?", u.Id).Order("id ASC").Select()
	if cmserr != nil{
		return nil, cmserr
	}
	
	claimSlice := make([]*pb.Claim, len(cmods))

	for i, claim := range cmods {
		if claim.Valid{
			claimSlice[i] = &pb.Claim{Id: claim.Id.String(), ClaimType: claim.ClaimType, Claim: claim.Claim, Valid: claim.Valid}
		}
	}

	address := &pb.StreetAddress{Id: u.Address.Id.String(), Data: u.Address.Data}

	personaData.PersonaId = u.PersonaId.String()
	personaData.Biography = u.Biography 
	personaData.FullName = u.FullName
	personaData.FirstName = u.FirstName
	personaData.LastName = u.LastName
	personaData.PhoneNumber = u.PhoneNumber
	personaData.Avatar = u.Avatar
	personaData.Address = address
	personaData.AssociatedClaims = claimSlice

	return personaData, nil
}

func (s *Server) CreatePersonaData(ctx context.Context, personaData *pb.PersonaData) (*pb.PersonaData, error) {
	createPersonaData := func(personaData *pb.PersonaData, personaId uuid.UUID) (error, string, *models.PersonaData) {  

		var table string
				tx, err := s.db.Begin()
				if err != nil {
					return err, table, nil
				}
				defer tx.Rollback()


				var newAddress *models.StreetAddress
				if personaData.Address != nil {
					newAddress = &models.StreetAddress{Data: personaData.Address.Data}
					_, pgerr := tx.Model(newAddress).Returning("*").Insert()
					if pgerr != nil {
						return pgerr, "street_address", nil
					}
				}


				newPersonaData := &models.PersonaData{
				    PersonaId: personaId,
				    Biography: personaData.Biography,
				    FullName: personaData.FullName,
				    FirstName: personaData.FirstName,
				    LastName: personaData.LastName,
					PhoneNumber: personaData.PhoneNumber,
					Avatar: personaData.Avatar,
					AddressId: newAddress.Id,

			}
			_, pgerr := tx.Model(newPersonaData).Returning("*").Insert()
			if pgerr != nil {
				return pgerr, "persona_data", nil
			}


			// Building response
			personaData.Address.Id = newPersonaData.AddressId.String()


			return tx.Commit(), table, newPersonaData
		}

		requiredErr := checkRequiredAttributes(personaData)
		if requiredErr != nil {
			return nil, requiredErr
		}

		personaId, err := internal.GetUuidFromString(personaData.PersonaId)
		if err != nil {
			return nil, err
		}

		pgerr, table, newPersonaData := createPersonaData(personaData, personaId)
		if pgerr != nil {
			return nil, internal.CheckError(pgerr, table)
		}


		newClaim := &pb.Claim{
			IssuingPersonaId: personaId.String(), 
			DataId: newPersonaData.Id.String(),
		}

		newClaimModel := &models.Claim{
			ClaimType: "data",
			Valid: true,
		}
		
		
		cerr, _ := newClaimModel.Create(s.db, newClaim); if cerr != nil{
			return nil, nil
		}
		

		newClaim.Id = newClaimModel.Id.String()




		var claims []*pb.Claim
		claims = append(claims, newClaim)


		return &pb.PersonaData{
			Id: newPersonaData.Id.String(),
			PersonaId: newPersonaData.PersonaId.String(),
			Biography: newPersonaData.Biography,
			FullName: newPersonaData.FullName,
			FirstName: newPersonaData.FirstName,
			LastName: newPersonaData.LastName,
			PhoneNumber: newPersonaData.PhoneNumber,
			Address: personaData.Address,
			Avatar: newPersonaData.Avatar,
			AssociatedClaims: claims,

		}, nil
	}




func (s *Server) UpdatePersonaData(ctx context.Context, personaData *pb.PersonaData) (*pb.Empty, error) {
	updatePersonaData := func(personaData *pb.PersonaData, u *models.PersonaData) (error, string) {
			var table string
			tx, err := s.db.Begin()
			if err != nil {
				return err, "persona_data"
			}
			defer tx.Rollback()



			//revoke claim(s)
			var cmods []models.Claim
			cmserr := tx.Model(&cmods).Where("data_id = ?", u.Id).Order("id ASC").Select()
			if cmserr != nil{
				return cmserr, "claim"
			}

			for _, claim := range cmods{

				claim.Valid = false
				clerr, _ := claim.Update(s.db)
				if clerr != nil{
					return clerr, "claim"
				}
			}

			// create new claim
			newClaim := &pb.Claim{
				IssuingPersonaId: personaData.PersonaId,
				DataId: personaData.Id,
				Valid: true, 
				ClaimType: "data",
			}

			newClaimModel := &models.Claim{
				ClaimType: "data",
				Valid: true,
			}
			
			
			cerr, _ := newClaimModel.Create(s.db, newClaim)
			if cerr != nil{
				return nil, ""
			}

			// Update address
			addressId, twerr := internal.GetUuidFromString(personaData.Address.Id)
			if twerr != nil {
				return twerr, "street_address"
			}

			address := &models.StreetAddress{Id: addressId, Data: personaData.Address.Data}
			_, pgerr := tx.Model(address).Column("data").WherePK().Update()

			if pgerr != nil {
				return pgerr, "street_address"
			}


			u.UpdatedAt = time.Now()
			_, pgerr = tx.Model(u).WherePK().Returning("*").UpdateNotNull()
			if pgerr != nil {
				return pgerr, "persona_data"
			}
			

		return tx.Commit(), table

	}

	err := checkRequiredAttributes(personaData)
	if err != nil {
		return nil, err
	}


	u, err := getPersonaDataModel(personaData)
		if err != nil {
			return nil, err
		}

	if pgerr, table := updatePersonaData(personaData, u); pgerr != nil {
    return nil, internal.CheckError(pgerr, table)
  }

  return &pb.Empty{}, nil
}

func (s *Server) DeletePersonaData(ctx context.Context, personaData *pb.PersonaData) (*pb.Empty, error) {
	id, twerr := internal.GetUuidFromString(personaData.Id)
	if twerr != nil {
		return nil, twerr
	}

	t := &models.PersonaData{Id: id}

	tx, err := s.db.Begin()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}
	defer tx.Rollback()

	//delete claims
	var cs []models.Claim

	cerr := tx.Model(&cs).Where("data_id = ?", id).Order("id ASC").Select()
	if cerr != nil{
		return nil, cerr
	}
	for _, claim := range cs{
		fmt.Printf("Deleting Claim:")
		fmt.Printf(claim.Id.String())
		claim.Delete(tx)
	}


	if pgerr, table := t.Delete(tx); pgerr != nil {
		return nil, internal.CheckError(pgerr, table)
	}
	err = tx.Commit()


	return &pb.Empty{}, nil
}




func getPersonaDataModel(personaData *pb.PersonaData) (*models.PersonaData, twirp.Error) {
  id, err := internal.GetUuidFromString(personaData.Id)
  if err != nil {
    return nil, err
  }
  
  personaId, err := internal.GetUuidFromString(personaData.PersonaId)
  if err != nil {
    return nil, err
  }
  
  addressId, err := internal.GetUuidFromString(personaData.Address.Id)
  if err != nil {
    return nil, err
  } 

  return &models.PersonaData{
  	Id: id,
    PersonaId: personaId,
    Biography: personaData.Biography,
    FullName: personaData.FullName,
    FirstName: personaData.FirstName,
    LastName: personaData.LastName,
	PhoneNumber: personaData.PhoneNumber,
	Avatar: personaData.Avatar,
	AddressId: addressId,
  }, nil
}



func checkRequiredAttributes(personaData *pb.PersonaData) (twirp.Error) {
	if personaData.FullName == "" || personaData.FirstName == "" || personaData.LastName == "" || personaData.Address.Data == nil || personaData.PersonaId == "" || personaData.Avatar == nil{ 
		var argument string
		switch {
		case personaData.FullName == "":
			argument = "full_name"
		case personaData.FirstName == "":
			argument = "first_name"
		case personaData.LastName == "":
			argument = "last_name"
		case personaData.Address.Data == nil:
			argument = "address_data"
		case personaData.PersonaId == "":
			argument = "persona_id"
		case personaData.Avatar == nil:
			argument = "avatar"
		}
		return twirp.RequiredArgumentError(argument)
	}
	return nil
}

// get claim response with arr of claims
func GetClaims(claims []*models.Claim) ([]*pb.Claim, twirp.Error) {
  var claimsResponse []*pb.Claim
  if len(claims) > 0 {

    for _, claim := range claims {
      if claim.Valid{
        claimResponse := &pb.Claim{
          Id: claim.Id.String(),
          ClaimType: claim.ClaimType,
          Valid: claim.Valid,
        }

        claimsResponse = append(claimsResponse, claimResponse)
      }
    }
  }

  return claimsResponse, nil
}





