package personaserver

import (
	"fmt"
	// "reflect"
	"time"
	"context"
	//"net/url"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/persona"
	datapb "mycelia/rpc/personadata"
	
	"mycelia/internal"
	"mycelia/internal/database/models"
)

type Server struct {
	db *pg.DB
}

func NewServer(db *pg.DB) *Server {
	return &Server{db: db}
}


func (s *Server) CreatePersona(ctx context.Context, persona *pb.Persona) (*pb.Persona, error) {
	requiredErr := checkRequiredAttributes(persona)
	if requiredErr != nil {
		return nil, requiredErr
	}

	creativePassportId, err := internal.GetUuidFromString(persona.CreativePassportId)
	if err != nil {
		return nil, err
	}
	serviceDidId, serr := internal.GetUuidFromString(persona.ServiceDidId)
	if serr != nil {
		return nil, serr
	}

	activeUser := true 

	newPersona := &models.Persona{
		Did: persona.Did,
		PublicKey: persona.PublicKey,
		PrivateKey: persona.PrivateKey,
		EthereumAddress: persona.EthereumAddress,
		Notes: persona.Notes,
		DidType: persona.DidType,
		Active: activeUser,
		Privacy: persona.Privacy,
		CreativePassportId: creativePassportId,
		ServiceDidId: serviceDidId,
	}



	_, therr := s.db.Model(newPersona).Returning("*").Insert()


	twerr := internal.CheckError(therr, "persona")
	if twerr != nil {
		return nil, twerr
	}

	newClaim := &datapb.Claim{
			IssuingPersonaId: newPersona.Id.String(), 
			SubjectServiceId: newPersona.ServiceDidId.String(),
			AssociatedServiceDidId: newPersona.ServiceDidId.String(),
		}

	newClaimModel := &models.Claim{
		ClaimType: "service",
		Valid: true,
	}	
		
	cerr, _ := newClaimModel.Create(s.db, newClaim); if cerr != nil{
		return nil, cerr
	}

	newClaim.Id = newClaimModel.Id.String()
	var claims []*datapb.Claim
	claims = append(claims, newClaim)

	newClaimSubject := &datapb.Claim{
		IssuingServiceId: newPersona.ServiceDidId.String(),
		SubjectPersonaId: newPersona.Id.String(),
		AssociatedServiceDidId: newPersona.ServiceDidId.String(),
	}

	newClaimSubjectModel := &models.Claim{
		ClaimType: "service",
		Valid: true,
	}

	cerr, _ = newClaimSubjectModel.Create(s.db, newClaimSubject); if cerr != nil{
		return nil, cerr
	}

	newClaimSubject.Id = newClaimSubjectModel.Id.String()
	var claimSubject []*datapb.Claim
	claimSubject = append(claimSubject, newClaimSubject)
	
	return &pb.Persona{
		Id: newPersona.Id.String(),
		PublicKey: newPersona.PublicKey,
		PrivateKey: newPersona.PrivateKey,
		EthereumAddress: newPersona.EthereumAddress,
		Notes: newPersona.Notes, 
		CreativePassportId: newPersona.CreativePassportId.String(),
		DidType: newPersona.DidType,
		Active: newPersona.Active,
		Privacy: newPersona.Privacy,
		ServiceDidId: newPersona.ServiceDidId.String(),
		IssuedServiceClaims: claims,
		SubjectServiceClaims: claimSubject,
	}, nil
}



// TODO handle privacy settings
func (s *Server) GetPersona(ctx context.Context, persona *pb.Persona) (*pb.Persona, error) {
	id, err := internal.GetUuidFromString(persona.Id)
	if err != nil {
		return nil, err
	}
	u := &models.Persona{Id: id}

	pgerr := s.db.Model(u).
		Column("persona.*", "AssociatedData").
		WherePK().
		Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "persona")
	}

	// Get social and service claims associated wtih persona

	var issuedSocialClaims []*datapb.Claim
	var subjectSocialClaims []*datapb.Claim

	var claimsIssued []models.Claim
	cmserr := s.db.Model(&claimsIssued).Where("issuing_persona_id = ?", id).Where("valid", true).Select()
	if cmserr != nil{
		return nil, cmserr
	}

	for i, claim := range claimsIssued {
		if claim.ClaimType == "social" {
			issuedSocialClaims[i] = &datapb.Claim{Id: claim.Id.String(), ClaimType: claim.ClaimType, Claim: claim.Claim, Valid: claim.Valid}
		}
	}

	var claimsSubject []models.Claim
	cmserr = s.db.Model(&claimsSubject).Where("subject_persona_id = ?", id).Where("valid", true).Select()


	for i, claim := range claimsSubject{
		if claim.ClaimType == "social"{
			subjectSocialClaims[i] = &datapb.Claim{Id: claim.Id.String(), ClaimType: claim.ClaimType, Claim: claim.Claim, Valid: claim.Valid}
		}
	}

	data, derr := getPersonaDataResponse("owner", u.AssociatedData)
	if derr != nil{
		return nil, derr
	}

	return &pb.Persona{
		Id: u.Id.String(),
		Did: u.Did,
		PrivateKey: u.PrivateKey,
		PublicKey: u.PublicKey,
		EthereumAddress: u.EthereumAddress,
		Notes: u.Notes,
		Active: u.Active,
		CreativePassportId: u.CreativePassportId.String(),
		ServiceDidId: u.ServiceDidId.String(),
		DidType: u.DidType,
		AssociatedData: data,
		IssuedSocialClaims: issuedSocialClaims,
		SubjectSocialClaims: subjectSocialClaims,
	}, nil
}

func (s *Server) UpdatePersona(ctx context.Context, persona *pb.Persona) (*datapb.Empty, error) {
	err := checkRequiredAttributes(persona)
	if err != nil {
		return nil, err
	}


	u, err := getPersonaModel(persona)
	if err != nil {
		return nil, err
	}

	
	u.UpdatedAt = time.Now()

	// revoke associated service claims
	var serviceClaims []models.Claim

	serr := s.db.Model(&serviceClaims).
	    Where("claim_type = ?", "service").
	    WhereGroup(func(q *orm.Query) (*orm.Query, error) {
	        q = q.WhereOr("issuing_persona_id = ?", u.Id).
	            WhereOr("subject_persona_id = ?", u.Id)
		return q, nil
	    }).
	    Select()
	if serr != nil {
		return nil, serr
	}


	for _, claim := range serviceClaims{
		claim.Valid = false
		clerr, _ := claim.Update(s.db)
		if clerr != nil{
			return nil, clerr
		}
	}

	// issue new service claims

	issueServiceClaim := &datapb.Claim{
				IssuingPersonaId: u.Id.String(),
				SubjectServiceId: u.ServiceDidId.String(),
				AssociatedServiceDidId: u.ServiceDidId.String(),
	}

	issueServiceClaimModel := &models.Claim{
		ClaimType: "service",
		Valid: true,
	}
	
	
	cerr, _ := issueServiceClaimModel.Create(s.db, issueServiceClaim)
	if cerr != nil{
		return nil, cerr
	}


	subjectServiceClaim := &datapb.Claim{
		IssuingServiceId: u.ServiceDidId.String(), 
		SubjectPersonaId: u.Id.String(), 
		AssociatedServiceDidId: u.ServiceDidId.String(), 
	}

	subjectServiceClaimModel := &models.Claim{
		ClaimType: "service",
		Valid: true,
	}
	
	
	cerr, _ = subjectServiceClaimModel.Create(s.db, subjectServiceClaim)
	if cerr != nil{
		return nil, cerr
	}


	_, pgerr := s.db.Model(u).WherePK().Returning("*").UpdateNotNull()
	twerr := internal.CheckError(pgerr, "persona")
	if twerr != nil {
		return nil, twerr
	}

	return &datapb.Empty{}, nil
}

func (s *Server) DeletePersona(ctx context.Context, persona *pb.Persona) (*datapb.Empty, error) {
	id, twerr := internal.GetUuidFromString(persona.Id)
	if twerr != nil {
		return nil, twerr
	}
	u := &models.Persona{Id: id}

	tx, err := s.db.Begin()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}
	defer tx.Rollback()


	//delete claims
	var cs []models.Claim

	cerr := tx.Model(&cs).
    WhereGroup(func(q *orm.Query) (*orm.Query, error) {
        q = q.WhereOr("issuing_persona_id = ?", id).
            WhereOr("subject_persona_id = ?", id)
	return q, nil
    }).
    Select()

	if cerr != nil{
		return nil, cerr
	}

	for _, claim := range cs{
		fmt.Printf("Deleting Claim:")
		fmt.Printf(claim.Id.String())
		err, _ = claim.Delete(tx)
		if err != nil{
			return nil, err
		}
	}


	if pgerr, table := u.Delete(tx); pgerr != nil {
		return nil, internal.CheckError(pgerr, table)
	}

	err = tx.Commit()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}

	

	return &datapb.Empty{}, nil
}


func (s *Server) CreateSocialGraphConnection(ctx context.Context, socialGraph *pb.SocialGraphConnection)(*pb.SocialGraphConnection, error){
	reqerr := checkRequiredSocialAttributes(socialGraph)
	if reqerr != nil{
		return nil, reqerr
	}

	claims, err := prepSocialGraph(socialGraph, s.db, "create")
	if err != nil{
		return nil, err
	}

	var claimResp []*datapb.Claim
	for _, claim := range claims{
		resp, rerr := claim.GetClaimResponse()
		if rerr != nil{
			return nil, rerr
		}

		claimResp = append(claimResp, resp)
	}

	return &pb.SocialGraphConnection{
		ReqSocialGraph: socialGraph.ReqSocialGraph,
		SubjSocialGraph: socialGraph.SubjSocialGraph,
		SocialGraphClaims: claimResp, 
		SocialGraphPriv: socialGraph.SocialGraphPriv,
	}, nil


}

func (s *Server) GetSocialGraphConnection(ctx context.Context, socialGraph *pb.SocialGraphConnection)(*pb.SocialGraphConnection, error){
	reqerr := checkRequiredSocialAttributes(socialGraph)
	if reqerr != nil{
		return nil, reqerr
	}

	claims, err := prepSocialGraph(socialGraph, s.db, "return_active")
	if err != nil{
		return nil, err
	}

	if len(claims) < 2{
		return nil, nil
	}


	subjPersonaId, serr := internal.GetUuidFromString(socialGraph.SubjSocialGraph[0].Id)
	if serr != nil{
		return nil, serr
	}
	u := &models.Persona{Id: subjPersonaId}

	rerr := checkPersonaValidity(subjPersonaId, s.db)
	if rerr != nil{
		return nil, rerr
	}

	pgerr := s.db.Model(u).
		Column("persona.*", "AssociatedData").
		WherePK().
		Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "persona")
	}



	persona, uerr := getPersonaResponse("social_graph", u, s.db)
	if uerr != nil{	
		fmt.Printf("No persona data!")
	}

	var personas []*pb.Persona
	personas = append(personas, persona)
	return &pb.SocialGraphConnection{
		ReqSocialGraph: socialGraph.ReqSocialGraph,
		SubjSocialGraph: persona, 
		SocialGraphPriv: socialGraph.SocialGraphPriv,
	}, nil
	


}

func (s *Server) UpdateSocialGraphConnection(ctx context.Context, socialGraph *pb.SocialGraphConnection)(*datapb.Empty, error){
	reqerr := checkRequiredSocialAttributes(socialGraph)
	if reqerr != nil{
		return nil, reqerr
	}

	_, err := prepSocialGraph(socialGraph, s.db, "create")
	if err != nil{
		return nil, err
	}

	return &datapb.Empty{}, nil

}

func (s *Server) DeleteSocialGraphConnection(ctx context.Context, socialGraph *pb.SocialGraphConnection)(*datapb.Empty, error){
	reqerr := checkRequiredSocialAttributes(socialGraph)
	if reqerr != nil{
		return nil, reqerr
	}

	tx, err := s.db.Begin()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}
	defer tx.Rollback()

	claims, err := prepSocialGraph(socialGraph, s.db, "return")
	if err != nil{
		return nil, err
	}

	for _, claim := range claims{
		err, _ := claim.Delete(tx)
		if err != nil{
			return nil, err
		}
	}

	err = tx.Commit()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}


	return &datapb.Empty{}, nil
}

func (s *Server) GetSocialGraphConnections(ctx context.Context, persona *pb.Persona)(*pb.PersonaSocialGraph, error){
	id, err := internal.GetUuidFromString(persona.Id)
	if err != nil{
		return nil, err
	}

	uerr := checkPersonaValidity(id, s.db)
	if uerr != nil{
		return nil, uerr
	}


	issuingUser := &models.Persona{Id: id}
	pgerr := s.db.Model(issuingUser).
		Column("persona.*", "AssociatedData").
		WherePK().
		Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "persona")
	}

	claims, clerr := getSocialGraph(issuingUser, s.db)
	if clerr != nil{
		return nil, clerr
	}

	var socialGraph []*pb.SocialGraphConnection
	for _, claim := range claims{
		if claim.Valid{
			

			u := &models.Persona{Id: claim.SubjectPersonaId}

			serr := s.db.Model(u).Column("persona.*", "AssociatedData").Where("active = ?", true).WherePK().Select()
			if serr != nil{
				return nil, serr
			}


			subjDidResp, rerr := getPersonaResponse("social_graph", u, s.db)
			if rerr != nil{
				fmt.Printf("No Persona Data!")
			}



			var personas []*pb.Persona
			personas = append(personas, persona)

			
			var subjDidResps []*pb.Persona
			subjDidResps = append(subjDidResps, subjDidResp)


			resp, reserr := getSocialGraphResponse(personas, subjDidResps, claim.SocialGraphPriv)
			 if reserr != nil{
			 	return nil, reserr
			 }
			fmt.Printf(resp.ReqSocialGraph[0].Id)
			socialGraph = append(socialGraph, resp)
		}


	}


	return &pb.PersonaSocialGraph{
		SocialGraphArray: socialGraph,
	}, nil



}

func (s *Server) GetPublicSocialGraph(ctx context.Context, persona *pb.Persona)(*pb.PublicSocialGraph, error){
	id, err := internal.GetUuidFromString(persona.Id)
	if err != nil{
		return nil, err
	}

	uerr := checkPersonaValidity(id, s.db)
	if uerr != nil{
		return nil, uerr
	}
	issuingUser := &models.Persona{Id: id}
	pgerr := s.db.Model(issuingUser).
		Column("persona.*", "AssociatedData").
		WherePK().
		Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "persona")
	}

	var socialGraph []*pb.Persona
	var socialGraphModels []*models.Persona
	serr := s.db.Model(&socialGraphModels).Where("privacy = false").Select()
	if serr != nil{
		return nil, serr
	}

	for _, mod := range socialGraphModels{
		resp, merr := getPersonaResponse("public", mod, s.db)
		if merr != nil{
			fmt.Printf("no Did data!")
		}
		socialGraph = append(socialGraph, resp)
	}

	return &pb.PublicSocialGraph{
		SocialGraphArray: socialGraph,
	}, nil

}

func prepSocialGraph(socialGraph *pb.SocialGraphConnection, db *pg.DB, command string)([]*models.Claim, error){
	id, err := internal.GetUuidFromString(socialGraph.ReqSocialGraph[0].Id)
	if err != nil {
		return nil, err
	}
	issuingUser := &models.Persona{Id: id}
	
	rerr := checkPersonaValidity(id, db)
	if rerr != nil{
		return nil, rerr
	}

	pgerr := db.Model(issuingUser).
		Column("persona.*").
		WherePK().
		Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "persona")
	}

	sid, serr := internal.GetUuidFromString(socialGraph.SubjSocialGraph[0].Id)
	if serr != nil {
		return nil, serr
	}

	rerr = checkPersonaValidity(sid, db)
	if rerr != nil{
		return nil, rerr
	}
	
	var claims []*models.Claim

	switch{
		case command == "create":
			return createSocialGraphClaims(id, sid, issuingUser.ServiceDidId, socialGraph.SocialGraphPriv, db)
		case command == "return":
			return getAllSharedClaims(id, sid, issuingUser.ServiceDidId, db)
		case command == "return_active":
			return getSharedActiveClaims(id, sid, issuingUser.ServiceDidId, db)

	}
	
	return claims, nil

}

func getPersonaModel(persona *pb.Persona) (*models.Persona, twirp.Error) {
	id, err := internal.GetUuidFromString(persona.Id)
	if err != nil {
		return nil, err
	}

	creativePassportId, err := internal.GetUuidFromString(persona.CreativePassportId)
	if err != nil {
		return nil, err
	}

	serviceDidId, err := internal.GetUuidFromString(persona.ServiceDidId)
	if err != nil {
		return nil, err
	}

	return &models.Persona{
		Id: id,
		Did: persona.Did,
		PrivateKey: persona.PrivateKey,
		PublicKey: persona.PublicKey,
		EthereumAddress: persona.EthereumAddress,
		Privacy: persona.Privacy,
		Active: persona.Active,
		DidType: persona.DidType,
		CreativePassportId: creativePassportId,
		ServiceDidId: serviceDidId,
	}, nil
}


func checkRequiredAttributes(persona *pb.Persona) (twirp.Error) {
	if persona.PublicKey == "" || persona.Did == "" || persona.PrivateKey == "" || persona.EthereumAddress == "" || persona.DidType == "" {
		var argument string
		switch {
		case persona.PublicKey == "":
			argument = "public_key"
		case persona.Did == "":
			argument = "did"
		case persona.PrivateKey == "":
			argument = "private_key"
		case persona.EthereumAddress == "":
			argument = "ethereum_address"
		case persona.DidType == "":
			argument = "did_type"
		}
		return twirp.RequiredArgumentError(argument)
	}
	return nil
}

func checkRequiredSocialAttributes(socialGraph *pb.SocialGraphConnection) (twirp.Error) {
	if socialGraph.ReqSocialGraph == nil || socialGraph.ReqSocialGraph[0] == nil || socialGraph.SubjSocialGraph == nil || len(socialGraph.SubjSocialGraph) == 0 || len(socialGraph.ReqSocialGraph) == 0 || socialGraph.SubjSocialGraph[0].Id == ""  {
		var argument string
		switch {
		case socialGraph.ReqSocialGraph == nil:
			argument = "req_social_graph"
		case socialGraph.SubjSocialGraph == nil:
			argument = "subj_social_graph"
		case len(socialGraph.ReqSocialGraph) == 0:
			argument = "req_social_graph_did"
		case len(socialGraph.SubjSocialGraph) == 0:
			argument = "subj_social_graph_did"
		case socialGraph.ReqSocialGraph[0].Id  == "":
			argument = "req_social_graph_did_id"
		case socialGraph.SubjSocialGraph[0].Id == "":
			argument = "subj_social_graph_did_id"
		}
		return twirp.RequiredArgumentError(argument)
	}
	return nil
}

func getSocialGraphResponse(personas []*pb.Persona, subjectDids []*pb.Persona, priv bool)(*pb.SocialGraphConnection, error){
		return &pb.SocialGraphConnection{
			ReqSocialGraph: personas, 
			SubjSocialGraph: subjectDids,
			SocialGraphPriv: priv,
		}, nil


}

func getPersonaResponse(sender string, persona *models.Persona, db *pg.DB) (*pb.Persona, error){
	var associatedData []models.PersonaData
	err := db.Model(&associatedData).Where("data_id = ?", persona.Id).Select()
	if err != nil{
		return &pb.Persona{
			Id: persona.Id.String(),
			Did: persona.Did, 
			PrivateKey: persona.PrivateKey,
			PublicKey: persona.PublicKey, 
			EthereumAddress: persona.EthereumAddress,
			DidType: persona.DidType,
			CreativePassportId: persona.CreativePassportId.String(),
			ServiceDidId: persona.ServiceDidId.String(),
		}, err

	}else{
		data, derr := getPersonaDataResponse(sender, associatedData)
		if derr != nil{
			return nil, derr
		}

		return &pb.Persona{
			Id: persona.Id.String(),
			Did: persona.Did, 
			PrivateKey: persona.PrivateKey,
			PublicKey: persona.PublicKey, 
			EthereumAddress: persona.EthereumAddress,
			DidType: persona.DidType,
			CreativePassportId: persona.CreativePassportId.String(),
			ServiceDidId: persona.ServiceDidId.String(),
			AssociatedData: data,
		}, nil
	}


}

func getPersonaDataResponse(sender string, associatedData []models.PersonaData) ([]*datapb.PersonaData, error) {
	datas := make([]*datapb.PersonaData, len(associatedData))
	for i, data := range associatedData {
		if sender == "owner" || sender == "social_graph" {
			datas[i] = &datapb.PersonaData{Id: data.Id.String(), FullName: data.FullName, Avatar: data.Avatar, FirstName: data.FirstName, LastName: data.LastName, PhoneNumber: data.PhoneNumber, BusinessEmail: data.BusinessEmail}

		}else{
			var dataRes *datapb.PersonaData
			if data.BiographyPriv{
				dataRes.Biography = data.Biography
			}

			if data.FullNamepriv{
				dataRes.FullName = data.FullName
			}

			if data.FirstNamePriv{
				dataRes.FirstName = data.FirstName
			}

			if data.LastNamePriv{
				dataRes.LastName = data.LastName
			}

			if data.PhoneNumberPriv{
				dataRes.PhoneNumber = data.PhoneNumber
			}

			if data.BusinessEmailPriv{
				dataRes.BusinessEmail = data.BusinessEmail
			}

			if data.AvatarPriv{
				dataRes.Avatar = data.Avatar
			}


			datas[i] = dataRes

		}	
	}
	return datas, nil
}

func createServiceClaims(persona *models.Persona)(*pb.Persona, *pb.Persona, error){
	fmt.Printf("on the way")
	return nil, nil, nil
}


func createSocialGraphClaims(persona uuid.UUID, secondPersona uuid.UUID, serviceDid uuid.UUID, priv bool, db *pg.DB)([]*models.Claim, error){
	claims, err := getSharedActiveClaims(persona, secondPersona, serviceDid, db)
	if err != nil{
		return nil, err
	}
	if len(claims) > 0{
		for _, claim := range claims{
			claim.Valid = false
			err, _ := claim.Update(db)
			if err != nil{
				return nil, err
			}
		}
	}

	newClaim := &datapb.Claim{
		IssuingPersonaId: persona.String(), 
		SubjectPersonaId: secondPersona.String(),
		AssociatedServiceDidId: serviceDid.String(),
	}

	newClaimSubj := &datapb.Claim{
		IssuingPersonaId: secondPersona.String(), 
		SubjectPersonaId: persona.String(),
		AssociatedServiceDidId: serviceDid.String(),
	}

	newClaimModel := &models.Claim{
		ClaimType: "social",
		Valid: true,
		SocialGraphPriv: priv,
	}

	newClaimSubjModel := &models.Claim{
		ClaimType: "social",
		Valid: true,
		SocialGraphPriv: priv,
	}	
	
	cerr, _ := newClaimModel.Create(db, newClaim); if cerr != nil{
		return nil, cerr
	}
	
	cerr, _ = newClaimSubjModel.Create(db, newClaimSubj); if cerr != nil{
		return nil, cerr
	}


	var finalClaims []*models.Claim
	finalClaims = append(finalClaims, newClaimModel, newClaimSubjModel)

	return finalClaims, nil



}



func checkPersonaValidity(u uuid.UUID, db *pg.DB)(twirp.Error){
  var claims []*models.Claim
  err := db.Model(&claims).
        Where("claim_type = ?", "service").
        Where("valid = ?", true).
        WhereGroup(func(q *orm.Query) (*orm.Query, error) {
            q = q.WhereOr("issuing_persona_id = ?", u).
                WhereOr("subject_persona_id = ?", u)
      return q, nil
        }).
        Select()
    if err != nil {
      return twirp.RequiredArgumentError("persona")
    }

    persona := &models.Persona{Id: u}
	pgerr := db.Model(persona).
			Column("persona.active").
			WherePK().
			Select()
		if pgerr != nil {
			return internal.CheckError(pgerr, "persona")
		}

   if persona.Active == false{
   		return twirp.RequiredArgumentError("not_active")
   }else if len(claims) < 2{
   		return twirp.RequiredArgumentError("no_claims")
   }else{
   		return nil
   }

}


func getSocialGraph(u *models.Persona, db *pg.DB)([]*models.Claim, error) {
  var claims []*models.Claim
  err := db.Model(&claims).
    Where("claim_type = ?", "social").
    Where("issuing_persona_id = ?", u.Id).
    Where("associated_service_did_id = ?", u.ServiceDidId).
    Select()
  if err != nil{
    return nil, err
  }

  return claims, nil
}


// Get Claims

func getAllSharedClaims(u uuid.UUID, subjPersona uuid.UUID, serviceDid uuid.UUID, db *pg.DB)([]*models.Claim, error){
	  var claims []*models.Claim
	  err := db.Model(&claims).
	    Where("claim_type = ?", "social").
	    Where("associated_service_did_id = ?", serviceDid).
	    Where("issuing_persona_id = ?", u).
	    Where("subject_persona_id = ?", subjPersona).
	    Select()
	  if err != nil{
	    return nil, err
	  }

	  var claimsSubj []*models.Claim
	  err = db.Model(&claimsSubj).
	    Where("claim_type = ?", "social").
	    Where("associated_service_did_id = ?", serviceDid).
	    Where("issuing_persona_id = ?", subjPersona).
	    Where("subject_persona_id = ?", u).
	    Select()
	  if err != nil{
	    return nil, err
	  }


	  claims = append(claims, claimsSubj...)

	  return claims, nil
}

func getSharedActiveClaims (u uuid.UUID, subjPersona uuid.UUID, serviceDid uuid.UUID, db *pg.DB) ([]*models.Claim, error) {
  var claims []*models.Claim
  err := db.Model(&claims).
    Where("claim_type = ?", "social").
    Where("valid = ?", true).
    Where("associated_service_did_id = ?", serviceDid).
    Where("issuing_persona_id = ?", u).
    Where("subject_persona_id = ?", subjPersona).
    Select()
  if err != nil{
    return nil, err
  }

  var claimsSubj []*models.Claim
  err = db.Model(&claimsSubj).
    Where("claim_type = ?", "social").
    Where("valid = ?", true).
    Where("associated_service_did_id = ?", serviceDid).
    Where("issuing_persona_id = ?", subjPersona).
    Where("subject_persona_id = ?", u).
    Select()
  if err != nil{
    return nil, err
  }


  claims = append(claims, claimsSubj...)

  return claims, nil


}

