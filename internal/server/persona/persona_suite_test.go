package personaserver_test

import (
	"testing"
	// "fmt"

	// pb "mycelia/rpc/creativepassport"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/go-pg/pg"

	"mycelia/internal/database"
	"mycelia/internal/database/models"
	personaserver "mycelia/internal/server/persona"
)

var (
	db *pg.DB
	service *personaserver.Server
	newCreativePassport *models.CreativePassport
	newPersona *models.Persona
	newPersonaData *models.PersonaData
	newService *models.Service
	newServiceDid *models.ServiceDid
	newServiceData *models.ServiceData
	newAddress *models.StreetAddress
	newClaimCreativePassport *models.Claim
	newClaimService *models.Claim
)

func TestCreativePassportgroup(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Persona server Suite")
}

var _ = BeforeSuite(func() {
		testing := true
		db = database.Connect(testing)
		service = personaserver.NewServer(db)

		// Create a new creativePassport (creativepassports table's empty)
		password := make([]byte, 5)
		newCreativePassport = &models.CreativePassport{Password: password, Email: "emuail@fake.com"}
		err := db.Insert(newCreativePassport)
		Expect(err).NotTo(HaveOccurred())
		
		//Create a new service

		newService = &models.Service{Username: "service name", Email: "service@service.com"}
		err = db.Insert(newService)
		Expect(err).NotTo(HaveOccurred())

		// Create a new service did

		newServiceDid = &models.ServiceDid{Did: "DIDString", PrivateKey: "PrivKeyString", PublicKey: "PubKeyString", EthereumAddress: "EthAddressString", Active: true, ServiceId: newService.Id, DidType:"service"}
		err = db.Insert(newServiceDid)
		Expect(err).NotTo(HaveOccurred())


		newAddress = &models.StreetAddress{Data: map[string]string{"some": "data"}}
		err = db.Insert(newAddress)
		Expect(err).NotTo(HaveOccurred())


		newPersona = &models.Persona{
			CreativePassportId: newCreativePassport.Id,
			ServiceDidId: newServiceDid.Id,
			Did: "DidString",
			PrivateKey: "PrivKeyString",
			PublicKey: "PubKeyString",
			EthereumAddress: "EthAddress",
			DidType: "persona",
			Privacy: false,
		}
		_, err = db.Model(newPersona).Returning("*").Insert()
		Expect(err).NotTo(HaveOccurred())

		avatar := make([]byte, 5)

		newPersonaData = &models.PersonaData{
		    PersonaId: newPersona.Id,
		    Biography: "short bio",
		    FullName: "TODO this should just concat first and last names",
		    FirstName: "Jenn",
		    LastName: "Fain",
			PhoneNumber: "01718415878",
			Avatar: avatar,
			AddressId: newAddress.Id,

		}
		_, err = db.Model(newPersonaData).Returning("*").Insert()
		Expect(err).NotTo(HaveOccurred())

		newClaimCreativePassport = &models.Claim{
			ClaimType: "service",
			IssuingCreativePassportId: newPersona.Id,
			SubjectServiceId: newServiceDid.Id,
			AssociatedServiceDidId: newServiceDid.Id,
			Valid: true,
		}
		err = db.Insert(newClaimCreativePassport)
		Expect(err).NotTo(HaveOccurred())		

		newClaimService = &models.Claim{
			ClaimType: "service",
			SubjectServiceId: newPersona.Id,
			IssuingServiceId: newServiceDid.Id,
			AssociatedServiceDidId: newServiceDid.Id,
			Valid: true,
		}
		err = db.Insert(newClaimService)
		Expect(err).NotTo(HaveOccurred())		

})

var _ = AfterSuite(func() {

	// delete all claims
	var claims []models.Claim
	err := db.Model(&claims).Select()
	if len(claims) > 0{
		_, err = db.Model(&claims).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	//delete all personaData
	var personaDatas [] models.PersonaData
	err = db.Model(&personaDatas).Select()
	Expect(err).NotTo(HaveOccurred())
	
	if len(personaDatas) > 0{
		_, err = db.Model(&personaDatas).Delete()
		Expect(err).NotTo(HaveOccurred())

	}

	// Delete all personas
	var personas []models.Persona
	err = db.Model(&personas).Select()
	Expect(err).NotTo(HaveOccurred())
	
	if len(personas) > 0{
		_, err = db.Model(&personas).Delete()
		Expect(err).NotTo(HaveOccurred())

	}

	// Delete all streetAddresses
	var streetAddresses []models.StreetAddress
	err = db.Model(&streetAddresses).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(streetAddresses) > 0{
		_, err = db.Model(&streetAddresses).Delete()
		Expect(err).NotTo(HaveOccurred())
	}



	// Delete all creativepassports
	var creativePassports []models.CreativePassport
	err = db.Model(&creativePassports).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(creativePassports) > 0{
			_, err = db.Model(&creativePassports).Delete()
			Expect(err).NotTo(HaveOccurred())
	}

	var serviceDids []models.ServiceDid
	err = db.Model(&serviceDids).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(serviceDids) > 0{
		_, err = db.Model(&serviceDids).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	var services []models.Service
	err = db.Model(&services).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(services) > 0{
		_, err = db.Model(&services).Delete()
		Expect(err).NotTo(HaveOccurred())
	}


})

