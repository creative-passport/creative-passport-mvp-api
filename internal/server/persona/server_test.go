package personaserver_test

import (
	// "fmt"
	// "reflect"
	"context"

	"github.com/go-pg/pg"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/persona"
	"mycelia/internal/database/models"
)

var _ = Describe("Persona server", func() {
  const already_exists_code twirp.ErrorCode = "already_exists"
  const invalid_argument_code twirp.ErrorCode = "invalid_argument"
  const not_found_code twirp.ErrorCode = "not_found"

	Describe("GetPersona", func() {
		Context("with valid uuid", func() {
			It("should respond with persona if it exists", func() {
				persona := &pb.Persona{Id: newPersona.Id.String()}
				resp, err := service.GetPersona(context.Background(), persona)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Id).To(Equal(newPersona.Id.String()))
				Expect(resp.Did).To(Equal(newPersona.Did))
				Expect(resp.PrivateKey).To(Equal(newPersona.PrivateKey))
				Expect(resp.PublicKey).To(Equal(newPersona.PublicKey))
				Expect(resp.EthereumAddress).To(Equal(newPersona.EthereumAddress))
				Expect(resp.Privacy).To(Equal(newPersona.Privacy))
				Expect(resp.Active).To(Equal(newPersona.Active))
				Expect(resp.DidType).To(Equal(newPersona.DidType))
				Expect(resp.CreativePassportId).To(Equal(newPersona.CreativePassportId.String()))
				Expect(resp.ServiceDidId).To(Equal(newPersona.ServiceDidId.String()))


				Expect(len(resp.AssociatedData)).To(Equal(1))
				Expect(resp.AssociatedData[0].Id).To(Equal(newPersonaData.Id.String()))
				Expect(resp.AssociatedData[0].FullName).To(Equal(newPersonaData.FullName))
			})
			It("should respond with not_found error if persona does not exist", func() {
				id := uuid.NewV4()
				for (id == newPersona.Id ) {
					id = uuid.NewV4()
				}
				persona := &pb.Persona{Id: id.String()}
				resp, _ := service.GetPersona(context.Background(), persona)

				Expect(resp).To(BeNil())

				//twerr := err.(twirp.Error)
				//Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				persona := &pb.Persona{Id: id}
				resp, err := service.GetPersona(context.Background(), persona)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("UpdatePersona", func() {
		Context("with valid uuid", func() {
			It("should update persona if it exists", func() {
				persona := &pb.Persona{
					Id: newPersona.Id.String(),
					Did: "newdid",
					PrivateKey: "newprivatekey",
					PublicKey: "newpublickey",
					DidType: "persona",
					EthereumAddress: newPersona.EthereumAddress,
					Privacy: newPersona.Privacy,
					CreativePassportId: newPersona.CreativePassportId.String(),
					ServiceDidId: newPersona.ServiceDidId.String(),
				}
				_, err := service.UpdatePersona(context.Background(), persona)

				Expect(err).NotTo(HaveOccurred())



				updatedPersona := new(models.Persona)
				err = db.Model(updatedPersona).Where("id = ?", newPersona.Id).Select()
				Expect(err).NotTo(HaveOccurred())

			})
			It("should respond with not_found error if persona does not exist", func() {
				id := uuid.NewV4()
				for (id == newPersona.Id ) {
					id = uuid.NewV4()
				}
				persona := &pb.Persona{
					Id: id.String(),
					Did: newPersona.Did,
					PrivateKey: newPersona.PrivateKey,
					PublicKey: newPersona.PublicKey,
					DidType: newPersona.EthereumAddress,
					EthereumAddress: newPersona.EthereumAddress,
					Privacy: newPersona.Privacy,
					CreativePassportId: newPersona.CreativePassportId.String(),
					ServiceDidId: newPersona.ServiceDidId.String(),
				}
				resp, err := service.UpdatePersona(context.Background(), persona)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				//twerr := err.(twirp.Error)
				//Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				persona := &pb.Persona{
					Id: id,
					Did: newPersona.Did,
					PrivateKey: newPersona.PrivateKey,
					PublicKey: newPersona.PublicKey,
					DidType: newPersona.EthereumAddress,
					EthereumAddress: newPersona.EthereumAddress,
					Privacy: newPersona.Privacy,
					CreativePassportId: newPersona.CreativePassportId.String(),
					ServiceDidId: newPersona.ServiceDidId.String(),
				}
				resp, err := service.UpdatePersona(context.Background(), persona)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	

	Describe("DeletePersona", func() {
		Context("with valid uuid", func() {
			It("should delete persona if it exists", func() {
				persona := &pb.Persona{Id: newPersona.Id.String()}

				personaToDelete := new(models.Persona)
				err := db.Model(personaToDelete).
					Column("AssociatedData", "persona.*").
					Where("id = ?", newPersona.Id).Select()
				Expect(err).NotTo(HaveOccurred())

				_, err = service.DeletePersona(context.Background(), persona)
				Expect(err).NotTo(HaveOccurred())




				var associatedData []models.PersonaData
				err = db.Model(&associatedData).
					Where("persona_id = ?", newPersona.Id).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(associatedData)).To(Equal(0))


				var personas []models.Persona
				err = db.Model(&personas).
					Where("id in (?)", pg.In([]uuid.UUID{personaToDelete.Id})).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(personas)).To(Equal(0))
			})
			It("should respond with not_found error if persona does not exist", func() {
				id := uuid.NewV4()
				for (id == newPersona.Id) {
					id = uuid.NewV4()
				}
				persona := &pb.Persona{Id: id.String()}
				resp, err := service.DeletePersona(context.Background(), persona)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				persona := &pb.Persona{Id: id}
				resp, err := service.DeletePersona(context.Background(), persona)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

  Describe("CreatePersona", func() {
		Context("with all required attributes", func() {
			It("should create a new persona", func() {
	
				creaivePassportId := newCreativePassport.Id.String()
				serviceDidId := newServiceDid.Id.String()

				persona := &pb.Persona{
					Did: "newdid",
					PrivateKey: "newprivatekey",
					PublicKey: "newpublickey",
					DidType: "persona",
					EthereumAddress: "newethereumaddress",
					Privacy: true,
					CreativePassportId: creaivePassportId,
					ServiceDidId: serviceDidId,
				}
				resp, err := service.CreatePersona(context.Background(), persona)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Id).NotTo(Equal(""))
				Expect(resp.PrivateKey).To(Equal("newprivatekey"))
				Expect(resp.PublicKey).To(Equal("newpublickey"))
				Expect(resp.EthereumAddress).To(Equal("newethereumaddress"))
				Expect(resp.DidType).To(Equal("persona"))
				Expect(resp.CreativePassportId).To(Equal(creaivePassportId))
				Expect(resp.ServiceDidId).To(Equal(serviceDidId))

				id, err := uuid.FromString(resp.Id)
				Expect(err).NotTo(HaveOccurred())
				updatedPersona := new(models.Persona)
				err = db.Model(updatedPersona).Where("id = ?", id).Select()
				Expect(err).NotTo(HaveOccurred())

			})

			It("should not create a persona with same did", func() {
				creaivePassportId := newCreativePassport.Id.String()
				serviceDidId := newServiceDid.Id.String()
				persona := &pb.Persona{
					Did: "newdid",
					PrivateKey: "secondprivkey",
					PublicKey: "secondpubkey",
					DidType: "persona",
					EthereumAddress: "secondethadd",
					Privacy: true,
					CreativePassportId: creaivePassportId,
					ServiceDidId: serviceDidId,
				}
				resp, err := service.CreatePersona(context.Background(), persona)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(already_exists_code))
				Expect(twerr.Msg()).To(Equal("did"))
			})
		})

		Context("with missing required attributes", func() {
			It("should not create a persona without did", func() {
				creaivePassportId := newCreativePassport.Id.String()
				serviceDidId := newServiceDid.Id.String()
				persona := &pb.Persona{
					PrivateKey: "secondprivkey",
					PublicKey: "secondpubkey",
					DidType: "persona",
					EthereumAddress: "secondethadd",
					Privacy: true,
					CreativePassportId: creaivePassportId,
					ServiceDidId: serviceDidId,
				}
				resp, err := service.CreatePersona(context.Background(), persona)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("did"))

			})


		})
  })

})
