package creativepassportserver_test

import (
	"testing"
	//"time"
	"fmt"
	// pb "mycelia/rpc/creative_passport"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/go-pg/pg"
	//"github.com/satori/go.uuid"

	"mycelia/internal/database"
	"mycelia/internal/database/models"
	creative_passportserver "mycelia/internal/server/creative_passport"
)



var (
	db *pg.DB
	service *creative_passportserver.Server
	newCreativePassport *models.CreativePassport
	newPersona *models.Persona
	newService *models.Service
	newServiceDid *models.ServiceDid

)

func TestCreativePassport(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "CreativePassport server Suite")
}

var _ = BeforeSuite(func() {
		testing := true
		db = database.Connect(testing)
		service = creative_passportserver.NewServer(db)

		newAddress := &models.StreetAddress{Data: map[string]string{"some": "data"}}
		err := db.Insert(newAddress)
		Expect(err).NotTo(HaveOccurred())

		// Create a new creative_passport
		password := make([]byte, 5)

		newCreativePassport = &models.CreativePassport{Password: password, Email: "email@fake.com", Admin: false}
		err = db.Insert(newCreativePassport)
		Expect(err).NotTo(HaveOccurred())

		//Create a new service

		newService = &models.Service{Username: "service", Email: "service@service.com"}
		err = db.Insert(newService)
		Expect(err).NotTo(HaveOccurred())
		// Create a new service did

		newServiceDid = &models.ServiceDid{Did: "DIDString", PrivateKey: "PrivKeyString", PublicKey: "PubKeyString", EthereumAddress: "EthAddressString", Active: true, ServiceId: newService.Id, DidType:"service"}
		err = db.Insert(newServiceDid)
		Expect(err).NotTo(HaveOccurred())
		
		// Create a new persona
		newPersona = &models.Persona{
			CreativePassportId: newCreativePassport.Id,
			ServiceDidId: newServiceDid.Id,
			Did: "DidString",
			PrivateKey: "PrivKeyString",
			PublicKey: "PubKeyString",
			EthereumAddress: "EthAddress",
			DidType: "persona",
			Privacy: true,

		}
		err = db.Insert(newPersona)
		Expect(err).NotTo(HaveOccurred())
})

var _ = AfterSuite(func() {
	// delete all claims
	var claims []models.Claim
	err := db.Model(&claims).Select()
	if len(claims) > 0{
		_, err = db.Model(&claims).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	// Delete all streetAddresses
	var streetAddresses []models.StreetAddress
	err = db.Model(&streetAddresses).Select()
	Expect(err).NotTo(HaveOccurred())
	fmt.Printf("Street Addresses")
	if len(streetAddresses) > 0{
			_, err = db.Model(&streetAddresses).Delete()
			Expect(err).NotTo(HaveOccurred())
	}

	//Delete all creative_passportData
	var personaData []models.PersonaData
	err = db.Model(&personaData).Select()
	Expect(err).NotTo(HaveOccurred())
	fmt.Printf("Persona Data")
	if len(personaData) > 0{
		_, err = db.Model(&personaData).Delete()
		Expect(err).NotTo(HaveOccurred())		
	}

	// delete all service data

	var serviceData []models.ServiceData
	err = db.Model(&serviceData).Select()
	Expect(err).NotTo(HaveOccurred())
	fmt.Printf("serviceData")
	if len(serviceData) >  0{
		_, err = db.Model(&serviceData).Delete()
		Expect(err).NotTo(HaveOccurred())
	}
	
	// Delete all personas
	var personas []models.Persona
	err = db.Model(&personas).Select()
	Expect(err).NotTo(HaveOccurred())
	
	if len(personas) > 0{
		_, err = db.Model(&personas).Delete()
		Expect(err).NotTo(HaveOccurred())
	}	


	// Delete all service dids
	var serviceDids []models.ServiceDid
	err = db.Model(&serviceDids).Select()
	Expect(err).NotTo(HaveOccurred())
	
	if len(serviceDids) > 0{
		_, err = db.Model(&serviceDids).Delete()
		Expect(err).NotTo(HaveOccurred())
	}
	// Delete all services
	var services []models.Service
	err = db.Model(&services).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(services) > 0{
		_, err = db.Model(&services).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	// Delete all creative_passports
	var creative_passports []models.CreativePassport
	err = db.Model(&creative_passports).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(creative_passports) > 0{
			_, err = db.Model(&creative_passports).Delete()
			Expect(err).NotTo(HaveOccurred())
	}



})
