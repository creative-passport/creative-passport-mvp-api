package creativepassportserver

import (
	//"fmt"
	// "reflect"
	"time"
	"context"

	"github.com/go-pg/pg"
	"github.com/twitchtv/twirp"
	//"github.com/satori/go.uuid"
	pb "mycelia/rpc/creative_passport"
	personapb "mycelia/rpc/persona"
	datapb "mycelia/rpc/personadata"
	"mycelia/internal"
	"mycelia/internal/database/models"
)

// Server implements the CreativePassportService
type Server struct {
	db *pg.DB
}

// NewServer creates an instance of our server
func NewServer(db *pg.DB) *Server {
	return &Server{db: db}
}

func (s *Server) GetCreativePassport(ctx context.Context, creative_passport *pb.CreativePassport) (*pb.CreativePassport, error) {
	u, err := getCreativePassportModel(creative_passport)
	if err != nil {
		return nil, err
	}

	pgerr := s.db.Model(u).
      Column("creative_passport.*", "AssociatedPersonas").
			Where("id = ?", u.Id).
      Select()
	twerr := internal.CheckError(pgerr, "creative_passport")
	if twerr != nil {
		return nil, twerr
	}

	return &pb.CreativePassport{
		Id: u.Id.String(),
		Password: u.Password,
		Email: u.Email,
		CreatedAt: u.CreatedAt.String(),
		UpdatedAt: u.UpdatedAt.String(),
		AssociatedPersonas: getPersonaResponse(u.AssociatedPersonas),
	}, nil
}


func (s *Server) CreateCreativePassport(ctx context.Context, creative_passport *pb.CreativePassport) (*pb.CreativePassport, error) {
	requiredErr := checkRequiredAttributes(creative_passport)
	if requiredErr != nil {
		return nil, requiredErr
	}


	newCreativePassport := &models.CreativePassport{
		Password: creative_passport.Password,
		Email: creative_passport.Email,
	}
	_, err := s.db.Model(newCreativePassport).Returning("*").Insert()


	twerr := internal.CheckError(err, "creative_passport")
	if twerr != nil {
		return nil, twerr
	}



	return &pb.CreativePassport{
		Id: newCreativePassport.Id.String(),
		Password: newCreativePassport.Password,
		Email: newCreativePassport.Email,
		CreatedAt: newCreativePassport.CreatedAt.String(),
	}, nil
}


func (s *Server) UpdateCreativePassport(ctx context.Context, creative_passport *pb.CreativePassport) (*datapb.Empty, error) {

	u, err := getCreativePassportModel(creative_passport)
	if err != nil {
		return nil, err
	}


	if creative_passport.Email != ""{
		u.Email = creative_passport.Email
	}

	if creative_passport.Password != nil{
		u.Password = creative_passport.Password
	}

	
	u.UpdatedAt = time.Now()



	_, pgerr := s.db.Model(u).WherePK().Returning("*").UpdateNotNull()
	twerr := internal.CheckError(pgerr, "creative_passport")
	if twerr != nil {
		return nil, twerr
	}

	return &datapb.Empty{}, nil
}


func (s *Server) DeleteCreativePassport(ctx context.Context, creative_passport *pb.CreativePassport) (*datapb.Empty, error) {
	deleteCreativePassport := func(db *pg.DB, u *models.CreativePassport) (error, string) {
		var table string
		tx, err := db.Begin()
		if err != nil {
			return err, table
		}
		defer tx.Rollback()

		pgerr := tx.Model(u).
	    Column("AssociatedPersonas").
	    WherePK().
	    Select()
		if pgerr != nil {
			return pgerr, "creative_passport"
		}


		if len(u.AssociatedPersonas) > 0 {
			for _, group := range u.AssociatedPersonas {
				if pgerr, table := group.Delete(tx); pgerr != nil {
					return pgerr, table
				}
			}

		}

		pgerr = tx.Delete(u)
		if pgerr != nil {
			return pgerr, "creative_passport"
		}

		return tx.Commit(), table
	}

	u, requiredErr := getCreativePassportModel(creative_passport)
	if requiredErr != nil {
		return nil, requiredErr
	}

	if pgerr, table := deleteCreativePassport(s.db, u); pgerr != nil {
		return nil, internal.CheckError(pgerr, table)
	}

	return &datapb.Empty{}, nil
}




func getCreativePassportModel(creative_passport *pb.CreativePassport) (*models.CreativePassport, twirp.Error) {
	id, err := internal.GetUuidFromString(creative_passport.Id)
	if err != nil {
		return nil, err
	}
	return &models.CreativePassport{
		Id: id,
	}, nil
}


func checkRequiredAttributes(creative_passport *pb.CreativePassport) (twirp.Error) {
	if creative_passport.Email == ""	|| creative_passport.Password == nil {
		var argument string
		switch {
		case creative_passport.Email == "":
			argument = "email"
		case creative_passport.Password == nil:
			argument = "username"
		}
		return twirp.RequiredArgumentError(argument)
	}
	return nil
}


func getPersonaResponse(associatedPersonas []models.Persona) ([]*personapb.Persona) {
	dids := make([]*personapb.Persona, len(associatedPersonas))
	for i, did := range associatedPersonas {
		data := getCreativePassportDataResponse(did.AssociatedData)
		dids[i] = &personapb.Persona{Id: did.Id.String(), PublicKey: did.PublicKey, PrivateKey: did.PrivateKey, AssociatedData: data}

	}
	return dids
}


func getCreativePassportDataResponse(associatedData []models.PersonaData) ([]*datapb.PersonaData) {
	datas := make([]*datapb.PersonaData, len(associatedData))
	for i, data := range associatedData {
		datas[i] = &datapb.PersonaData{Id: data.Id.String(), FullName: data.FullName, Biography: data.Biography}

	}
	return datas
}