package creativepassportserver_test

import (
	// "fmt"
	// "reflect"
	"context"

	"github.com/go-pg/pg"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/creative_passport"
	"mycelia/internal/database/models"
)

var _ = Describe("CreativePassport server", func() {
	const already_exists_code twirp.ErrorCode = "already_exists"
	const invalid_argument_code twirp.ErrorCode = "invalid_argument"
	const not_found_code twirp.ErrorCode = "not_found"

	Describe("GetCreativePassport", func() {
		Context("with valid uuid", func() {
			It("should respond with creative_passport if it exists", func() {
				creative_passport := &pb.CreativePassport{Id: newCreativePassport.Id.String()}
				resp, err := service.GetCreativePassport(context.Background(), creative_passport)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Id).To(Equal(newCreativePassport.Id.String()))
				Expect(resp.Email).To(Equal(newCreativePassport.Email))

			})
			It("should respond with not_found error if creative_passport does not exist", func() {
				id := uuid.NewV4()
				for id == newCreativePassport.Id {
					id = uuid.NewV4()
				}
				creative_passport := &pb.CreativePassport{Id: id.String()}
				resp, err := service.GetCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				creative_passport := &pb.CreativePassport{Id: id}
				resp, err := service.GetCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("UpdateCreativePassport", func() {
		Context("with valid uuid", func() {
			It("should update creative_passport if it exists", func() {
				password := make([]byte, 5)
				creative_passport := &pb.CreativePassport{
					Id: newCreativePassport.Id.String(),
					Password: password,
					Email: "email@fake.com",
				}
				_, err := service.UpdateCreativePassport(context.Background(), creative_passport)

				Expect(err).NotTo(HaveOccurred())
			})
			It("should respond with not_found error if creative_passport does not exist", func() {
				id := uuid.NewV4()
				for id == newCreativePassport.Id {
					id = uuid.NewV4()
				}
				password := make([]byte, 5)

				creative_passport := &pb.CreativePassport{
					Id: id.String(),
					Password: password,
					Email: "email@fake.comm",
				}
				resp, err := service.UpdateCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				password := make([]byte, 5)

				creative_passport := &pb.CreativePassport{
					Id: id,
					Password: password,
					Email: "email@fake.com",
				}
				resp, err := service.UpdateCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("CreateCreativePassport", func() {
		Context("with all required attributes", func() {
			It("should create a new creative_passport", func() {
				password := make([]byte, 5)

				creative_passport := &pb.CreativePassport{Password: password, Email: "jane@d.com"}
				resp, err := service.CreateCreativePassport(context.Background(), creative_passport)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Password).To(Equal("password"))
				Expect(resp.Email).To(Equal("jane@d.com"))
				Expect(resp.Id).NotTo(Equal(""))
			})

			It("should not create a creative_passport with same email", func() {
				password := make([]byte, 5)

				creative_passport := &pb.CreativePassport{Password: password, Email: "jane@d.com"}
				resp, err := service.CreateCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(already_exists_code))
				Expect(twerr.Msg()).To(Equal("email"))
			})


		})

		Context("with missing required attributes", func() {
			It("should not create a creative_passport without email", func() {
				password := make([]byte, 5)

				creative_passport := &pb.CreativePassport{Passowrd: password, Email: ""}
				resp, err := service.CreateCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("email"))
			})
			It("should not create a creative_passport without password", func() {
				password := make([]byte, 5)

				creative_passport := &pb.CreativePassport{Password: "", Email: "john@doe.com"}
				resp, err := service.CreateCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("password"))
			})
			
		})
	})

	Describe("DeleteCreativePassport", func() {
		Context("with valid uuid", func() {
			It("should delete creative_passport if it exists", func() {
				creative_passport := &pb.CreativePassport{Id: newCreativePassport.Id.String()}

				creative_passportToDelete := new(models.CreativePassport)
				err := db.Model(creative_passportToDelete).Column("creative_passport.*", "AssociatedPersonas").Where("id = ?", newCreativePassport.Id).Select()
				Expect(err).NotTo(HaveOccurred())

				_, err = service.DeleteCreativePassport(context.Background(), creative_passport)

				Expect(err).NotTo(HaveOccurred())

				//TODO cascade del persona, personadata, etc. 


				var creative_passports []models.CreativePassport
				err = db.Model(&creative_passports).
					Where("id in (?)", pg.In([]uuid.UUID{creative_passportToDelete.Id})).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(creative_passports)).To(Equal(0))
			})
			It("should respond with not_found error if creative_passport does not exist", func() {
				id := uuid.NewV4()
				for id == newCreativePassport.Id {
					id = uuid.NewV4()
				}
				creative_passport := &pb.CreativePassport{Id: id.String()}
				resp, err := service.DeleteCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				creative_passport := &pb.CreativePassport{Id: id}
				resp, err := service.DeleteCreativePassport(context.Background(), creative_passport)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})
})



