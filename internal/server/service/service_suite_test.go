package serviceserver_test

import (
	"testing"
	//"time"

	// pb "mycelia/rpc/user"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/go-pg/pg"
	//"github.com/satori/go.uuid"

	"mycelia/internal/database"
	"mycelia/internal/database/models"
	serviceserver "mycelia/internal/server/service"
)

var (
	db *pg.DB
	service *serviceserver.Server
	newService *models.Service
	newServiceDid *models.ServiceDid
	newUser *models.User
	newUsereDid *models.Persona
)

func TestService(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Service server Suite")
}

var _ = BeforeSuite(func() {
		testing := true
		db = database.Connect(testing)
		service = serviceserver.NewServer(db)

		newAddress := &models.StreetAddress{Data: map[string]string{"some": "data"}}
		err := db.Insert(newAddress)
		Expect(err).NotTo(HaveOccurred())

		//Create a new service
		newService = &models.Service{Username: "service name", Email: "service@service.com"}
		err = db.Insert(newService)
		Expect(err).NotTo(HaveOccurred())

		// Create a new service_did
		newServiceDid = &models.ServiceDid{
			ServiceId: newService.Id,
			Did: "DidString",
			PrivateKey: "PrivKeyString",
			PublicKey: "PubKeyString",
			EthereumAddress: "EthAddress",
			DidType: "service",
		}
		err = db.Insert(newServiceDid)
		Expect(err).NotTo(HaveOccurred())
})

var _ = AfterSuite(func() {

	// Delete all streetAddresses
	var streetAddresses []models.StreetAddress
	err := db.Model(&streetAddresses).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(streetAddresses) > 0{
		_, err = db.Model(&streetAddresses).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	// Delete all serviceDids
	var serviceDids []models.ServiceDid
	err = db.Model(&serviceDids).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(serviceDids) > 0 {
		_, err = db.Model(&serviceDids).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	// Delete all services
	var services []models.Service
	err = db.Model(&services).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(services) > 0{
		_, err = db.Model(&services).Delete()
		Expect(err).NotTo(HaveOccurred())
	}


})
