package serviceserver

import (
	//"fmt"
	// "reflect"
	"time"
	"context"

	"github.com/go-pg/pg"
	"github.com/twitchtv/twirp"
	//"github.com/satori/go.uuid"
	pb "mycelia/rpc/service"
	didpb "mycelia/rpc/servicedid"
	userdatapb "mycelia/rpc/personadata"

	"mycelia/internal"
	"mycelia/internal/database/models"
)

// Server implements the ServiceService
type Server struct {
	db *pg.DB
}

// NewServer creates an instance of our server
func NewServer(db *pg.DB) *Server {
	return &Server{db: db}
}

func (s *Server) GetService(ctx context.Context, service *pb.Service) (*pb.Service, error) {
	u, err := getServiceModel(service)
	if err != nil {
		return nil, err
	}

	pgerr := s.db.Model(u).
      Column("service.*", "OwnerOfDids").
			Where("id = ?", u.Id).
      Select()
	twerr := internal.CheckError(pgerr, "service")
	if twerr != nil {
		return nil, twerr
	}

	return &pb.Service{
		Id: u.Id.String(),
		Username: u.Username,
		Email: u.Email,
		CreatedAt: u.CreatedAt.String(),
		UpdatedAt: u.UpdatedAt.String(),
		OwnerOfDids: getServiceDidResponse(u.OwnerOfDids),
	}, nil
}


func (s *Server) CreateService(ctx context.Context, service *pb.Service) (*pb.Service, error) {
	requiredErr := checkRequiredAttributes(service)
	if requiredErr != nil {
		return nil, requiredErr
	}


	newService := &models.Service{
		Username: service.Username,
		Email: service.Email,
		Admin: service.Admin,
	}
	_, err := s.db.Model(newService).Returning("*").Insert()


	twerr := internal.CheckError(err, "service")
	if twerr != nil {
		return nil, twerr
	}



	return &pb.Service{
		Id: newService.Id.String(),
		Username: newService.Username,
		Email: newService.Email,
		Admin: newService.Admin,
		CreatedAt: newService.CreatedAt.String(),
	}, nil
}


func (s *Server) UpdateService(ctx context.Context, service *pb.Service) (*userdatapb.Empty, error) {
	err := checkRequiredAttributes(service)
	if err != nil {
		return nil, err
	}


	u, err := getServiceModel(service)
	if err != nil {
		return nil, err
	}

	if service.Email != ""{
		u.Email = service.Email
	}

	if service.Username != ""{
		u.Username = service.Username
	}

	
	u.UpdatedAt = time.Now()


	_, pgerr := s.db.Model(u).WherePK().Returning("*").UpdateNotNull()
	twerr := internal.CheckError(pgerr, "service")
	if twerr != nil {
		return nil, twerr
	}

	return &userdatapb.Empty{}, nil
}


func (s *Server) DeleteService(ctx context.Context, service *pb.Service) (*userdatapb.Empty, error) {
	deleteService := func(db *pg.DB, u *models.Service) (error, string) {
		var table string
		tx, err := db.Begin()
		if err != nil {
			return err, table
		}
		defer tx.Rollback()

		pgerr := tx.Model(u).
	    Column("OwnerOfDids").
	    WherePK().
	    Select()
		if pgerr != nil {
			return pgerr, "service"
		}


		
		for _, group := range u.OwnerOfDids {
			if pgerr, table := group.Delete(tx); pgerr != nil {
				return pgerr, table
			}
		}



		pgerr = tx.Delete(u)
		if pgerr != nil {
			return pgerr, "service"
		}

		return tx.Commit(), table
	}

	u, requiredErr := getServiceModel(service)
	if requiredErr != nil {
		return nil, requiredErr
	}

	if pgerr, table := deleteService(s.db, u); pgerr != nil {
		return nil, internal.CheckError(pgerr, table)
	}

	return &userdatapb.Empty{}, nil
}



func getServiceModel(service *pb.Service) (*models.Service, twirp.Error) {
	id, err := internal.GetUuidFromString(service.Id)
	if err != nil {
		return nil, err
	}
	return &models.Service{
		Id: id,
	}, nil

}

func checkRequiredAttributes(service *pb.Service) (twirp.Error) {
	if service.Email == ""	|| service.Username == "" {
		var argument string
		switch {
		case service.Email == "":
			argument = "email"
		case service.Username == "":
			argument = "username"
		}
		return twirp.RequiredArgumentError(argument)
	}
	return nil
}


func getServiceDidResponse(ownerOfDids []models.ServiceDid) ([]*didpb.ServiceDid) {
	dids := make([]*didpb.ServiceDid, len(ownerOfDids))
	for i, did := range ownerOfDids {
		dids[i] = &didpb.ServiceDid{Id: did.Id.String(), PublicKey: did.PublicKey, PrivateKey: did.PrivateKey}
	}
	return dids
}