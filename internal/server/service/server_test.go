package serviceserver_test

import (
	// "fmt"
	// "reflect"
	"context"

	"github.com/go-pg/pg"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/service"
	"mycelia/internal/database/models"
)

var _ = Describe("Service server", func() {
	const already_exists_code twirp.ErrorCode = "already_exists"
	const invalid_argument_code twirp.ErrorCode = "invalid_argument"
	const not_found_code twirp.ErrorCode = "not_found"

	Describe("GetService", func() {
		Context("with valid uuid", func() {
			It("should respond with service if it exists", func() {
				serviceReq := &pb.Service{Id: newService.Id.String()}
				resp, err := service.GetService(context.Background(), serviceReq)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Id).To(Equal(newService.Id.String()))
				Expect(resp.Email).To(Equal(newService.Email))

			})
			It("should respond with not_found error if service does not exist", func() {
				id := uuid.NewV4()
				for id == newService.Id {
					id = uuid.NewV4()
				}
				serviceReq := &pb.Service{Id: id.String()}
				resp, err := service.GetService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceReq := &pb.Service{Id: id}
				resp, err := service.GetService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("UpdateService", func() {
		Context("with valid uuid", func() {
			It("should update service if it exists", func() {
				serviceReq := &pb.Service{
					Id: newService.Id.String(),
					Username: "new servicename",
					Email: "email@fake.com",
				}
				_, err := service.UpdateService(context.Background(), serviceReq)

				Expect(err).NotTo(HaveOccurred())
			})
			It("should respond with not_found error if service does not exist", func() {
				id := uuid.NewV4()
				for id == newService.Id {
					id = uuid.NewV4()
				}
				serviceReq := &pb.Service{
					Id: id.String(),
					Username: "servicename",
					Email: "email@fake.comm",
				}
				resp, err := service.UpdateService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceReq := &pb.Service{
					Id: id,
					Username: "new servicename",
					Email: "email@fake.com",
				}
				resp, err := service.UpdateService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("CreateService", func() {
		Context("with all required attributes", func() {
			It("should create a new service", func() {
				serviceReq := &pb.Service{Username: "janed", Email: "jane@d.com"}
				resp, err := service.CreateService(context.Background(), serviceReq)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Username).To(Equal("janed"))
				Expect(resp.Email).To(Equal("jane@d.com"))
				Expect(resp.Id).NotTo(Equal(""))
			})

			It("should not create a service with same email", func() {
				serviceReq := &pb.Service{Username: "janedoe", Email: "jane@d.com"}
				resp, err := service.CreateService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(already_exists_code))
				Expect(twerr.Msg()).To(Equal("email"))
			})

			It("should not create a service with same username", func() {
				serviceReq := &pb.Service{Username: "janed", Email: "jane@doe.com"}
				resp, err := service.CreateService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(already_exists_code))
				Expect(twerr.Msg()).To(Equal("username"))
			})
		})

		Context("with missing required attributes", func() {
			It("should not create a service without email", func() {
				serviceReq := &pb.Service{Username: "johnd", Email: ""}
				resp, err := service.CreateService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("email"))
			})
			It("should not create a service without username", func() {
				serviceReq := &pb.Service{Username: "", Email: "john@doe.com"}
				resp, err := service.CreateService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("username"))
			})
			
		})
	})

	Describe("DeleteService", func() {
		Context("with valid uuid", func() {
			It("should delete service if it exists", func() {
				serviceReq := &pb.Service{Id: newService.Id.String()}

				serviceToDelete := new(models.Service)
				err := db.Model(serviceToDelete).Column("service.*", "OwnerOfDids").Where("id = ?", newService.Id).Select()
				Expect(err).NotTo(HaveOccurred())

				_, err = service.DeleteService(context.Background(), serviceReq)

				Expect(err).NotTo(HaveOccurred())

				//TODO cascade del servicedid, personadata, etc. 


				var services []models.Service
				err = db.Model(&services).
					Where("id in (?)", pg.In([]uuid.UUID{serviceToDelete.Id})).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(services)).To(Equal(0))
			})
			It("should respond with not_found error if service does not exist", func() {
				id := uuid.NewV4()
				for id == newService.Id {
					id = uuid.NewV4()
				}
				serviceReq := &pb.Service{Id: id.String()}
				resp, err := service.DeleteService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceReq := &pb.Service{Id: id}
				resp, err := service.DeleteService(context.Background(), serviceReq)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})
})



