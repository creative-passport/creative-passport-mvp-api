package servicedataserver_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/go-pg/pg"

	"mycelia/internal/database/models"
	"mycelia/internal/database"
	servicedataserver "mycelia/internal/server/servicedata"
)

var (
	db *pg.DB
	service *servicedataserver.Server
	newServiceData *models.ServiceData
	newService *models.Service
	newServiceDid *models.ServiceDid
	serviceDataAddress *models.StreetAddress

)

func TestServiceData(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "ServiceData server Suite")
}

var _ = BeforeSuite(func() {
	testing := true
	db = database.Connect(testing)
	service = servicedataserver.NewServer(db)


	serviceDataAddress := &models.StreetAddress{Data: map[string]string{"some": "data"}}
	err := db.Insert(serviceDataAddress)
	Expect(err).NotTo(HaveOccurred())

	newService := &models.Service{Username: "service", Email: "service@service.io"}
	err = db.Insert(newService)
	Expect(err).NotTo(HaveOccurred())


	newServiceDid = &models.ServiceDid{
		Did: "Did",
		PrivateKey: "PrivateKey",
		PublicKey: "PublicKey",
		DidType: "service",
		EthereumAddress: "EthAdd",
		Active: true, 
		ServiceId: newService.Id,
	}
	err = db.Insert(newServiceDid)
	Expect(err).NotTo(HaveOccurred())


	// Create a new persona_data
	avatar := make([]byte, 5)
	newServiceData = &models.ServiceData{
		Biography: "bio",
		FirstName: "Amy",
		LastName: "Jones",
		FullName: "Amy Jones",
		Avatar: avatar,
		ServiceDidId: newServiceDid.Id,
		AddressId: serviceDataAddress.Id,
	}
	err = db.Insert(newServiceData)
	Expect(err).NotTo(HaveOccurred())

})

var _ = AfterSuite(func() {
	// delete all claims
	var claims []models.Claim
	err := db.Model(&claims).Select()
	if len(claims) > 0{
		_, err = db.Model(&claims).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	// Delete all DID data

	var serviceData []models.ServiceData
	err = db.Model(&serviceData).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(serviceData) > 0{
		_, err = db.Model(&serviceData).Delete()
		Expect(err).NotTo(HaveOccurred())	
	}

	// Delete all addresses
	var addresses []models.StreetAddress
	err = db.Model(&addresses).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(addresses) > 0{
		_, err = db.Model(&addresses).Delete()
		Expect(err).NotTo(HaveOccurred())		
	}




	// Delete all serviceDids
	var serviceDids []models.ServiceDid
	err = db.Model(&serviceDids).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(serviceDids) > 0{
		_, err = db.Model(&serviceDids).Delete()
		Expect(err).NotTo(HaveOccurred())
	}


	// Delete all services
	var services []models.Service
	err = db.Model(&services).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(services) > 0{
		_, err = db.Model(&services).Delete()
		Expect(err).NotTo(HaveOccurred())
	}



})

