package servicedataserver

import (
	//"fmt"
	"time"
	"context"

	"github.com/go-pg/pg"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/servicedata"
	//servicedatapb "mycelia/rpc/servicedata"
	userdatapb "mycelia/rpc/personadata"

	"mycelia/internal"
	"mycelia/internal/database/models"
)

type Server struct {
	db *pg.DB
}

func NewServer(db *pg.DB) *Server {
	return &Server{db: db}
}

func (s *Server) GetServiceData(ctx context.Context, serviceData *pb.ServiceData) (*pb.ServiceData, error) {
	id, err := internal.GetUuidFromString(serviceData.Id)
	if err != nil {
		return nil, err
	}
	t := &models.ServiceData{Id: id}
	pgerr := s.db.Model(t).
			Column("service_data.*", "Address").
			WherePK().
			Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "service_data")
	}

	address := &pb.ServiceStreetAddress{Id: t.Address.Id.String(), Data: t.Address.Data}

	serviceData.ServiceDidId = t.ServiceDidId.String()
	serviceData.Biography = t.Biography 
	serviceData.FullName = t.FullName
	serviceData.FirstName = t.FirstName
	serviceData.LastName = t.LastName
	serviceData.PhoneNumber = t.PhoneNumber
	serviceData.Avatar = t.Avatar
	serviceData.Address = address

	return serviceData, nil
}

func (s *Server) CreateServiceData(ctx context.Context, serviceData *pb.ServiceData) (*pb.ServiceData, error) {
	createServiceData := func(serviceData *pb.ServiceData, serviceDidId uuid.UUID) (error, string, *models.ServiceData) {  

		var table string
				tx, err := s.db.Begin()
				if err != nil {
					return err, table, nil
				}
				defer tx.Rollback()


				var newAddress *models.StreetAddress
				if serviceData.Address != nil {
					newAddress = &models.StreetAddress{Data: serviceData.Address.Data}
					_, pgerr := tx.Model(newAddress).Returning("*").Insert()
					if pgerr != nil {
						return pgerr, "street_address", nil
					}
				}


				newServiceData := &models.ServiceData{
				    ServiceDidId: serviceDidId,
				    Biography: serviceData.Biography,
				    FullName: serviceData.FullName,
				    FirstName: serviceData.FirstName,
				    LastName: serviceData.LastName,
					PhoneNumber: serviceData.PhoneNumber,
					Avatar: serviceData.Avatar,
					AddressId: newAddress.Id,
			}
			_, pgerr := tx.Model(newServiceData).Returning("*").Insert()
			if pgerr != nil {
				return pgerr, "service_data", nil
			}


			// Building response
			serviceData.Address.Id = newServiceData.AddressId.String()

			return tx.Commit(), table, newServiceData
		}

		requiredErr := checkRequiredAttributes(serviceData)
		if requiredErr != nil {
			return nil, requiredErr
		}

		serviceDidId, err := internal.GetUuidFromString(serviceData.ServiceDidId)
		if err != nil {
			return nil, err
		}

		pgerr, table, newServiceData := createServiceData(serviceData, serviceDidId)
		if pgerr != nil {
			return nil, internal.CheckError(pgerr, table)
		}

	  return &pb.ServiceData{
			Id: newServiceData.Id.String(),
			ServiceDidId: newServiceData.ServiceDidId.String(),
			Biography: newServiceData.Biography,
			FullName: newServiceData.FullName,
			FirstName: newServiceData.FirstName,
			LastName: newServiceData.LastName,
			PhoneNumber: newServiceData.PhoneNumber,
			Address: serviceData.Address,
			Avatar: newServiceData.Avatar,
		}, nil
	}




func (s *Server) UpdateServiceData(ctx context.Context, serviceData *pb.ServiceData) (*userdatapb.Empty, error) {
	updateServiceData := func(serviceData *pb.ServiceData, u *models.ServiceData) (error, string) {
			var table string
			tx, err := s.db.Begin()
			if err != nil {
				return err, "service_data"
			}
			defer tx.Rollback()

			// Update address
			addressId, twerr := internal.GetUuidFromString(serviceData.Address.Id)
			if twerr != nil {
				return twerr, "street_address"
			}
			address := &models.StreetAddress{Id: addressId, Data: serviceData.Address.Data}
			_, pgerr := tx.Model(address).Column("data").WherePK().Update()
			// _, pgerr := db.Model(address).Set("data = ?", pg.Hstore(userGroup.Address.Data)).Where("id = ?id").Update()
			if pgerr != nil {
				return pgerr, "street_address"
			}


			u.UpdatedAt = time.Now()
			_, pgerr = tx.Model(u).WherePK().Returning("*").UpdateNotNull()
			if pgerr != nil {
				return pgerr, "service_data"
			}
		
		return tx.Commit(), table

	}

	err := checkRequiredAttributes(serviceData)
	if err != nil {
		return nil, err
	}


	u, err := getServiceDataModel(serviceData)
		if err != nil {
			return nil, err
		}

	if pgerr, table := updateServiceData(serviceData, u); pgerr != nil {
    return nil, internal.CheckError(pgerr, table)
  }

  return &userdatapb.Empty{}, nil
}

func (s *Server) DeleteServiceData(ctx context.Context, serviceData *pb.ServiceData) (*userdatapb.Empty, error) {
	id, twerr := internal.GetUuidFromString(serviceData.Id)
	if twerr != nil {
		return nil, twerr
	}

	t := &models.ServiceData{Id: id}

	tx, err := s.db.Begin()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}
	defer tx.Rollback()

	if pgerr, table := t.Delete(tx); pgerr != nil {
		return nil, internal.CheckError(pgerr, table)
	}
	err = tx.Commit()
  if err != nil {
    return nil, internal.CheckError(err, "")
  }
	return &userdatapb.Empty{}, nil
}

func getServiceDataModel(serviceData *pb.ServiceData) (*models.ServiceData, twirp.Error) {

  	id, err := internal.GetUuidFromString(serviceData.Id)
  	if err != nil {
    	return nil, err
  	}
  
  serviceDidId, err := internal.GetUuidFromString(serviceData.ServiceDidId)
  if err != nil {
    return nil, err
  }
  
  addressId, err := internal.GetUuidFromString(serviceData.Address.Id)
  if err != nil {
    return nil, err
  } 

  return &models.ServiceData{
  	Id: id,
    ServiceDidId: serviceDidId,
    Biography: serviceData.Biography,
    FullName: serviceData.FullName,
    FirstName: serviceData.FirstName,
    LastName: serviceData.LastName,
	PhoneNumber: serviceData.PhoneNumber,
	Avatar: serviceData.Avatar,
	AddressId: addressId,
  }, nil
}



func checkRequiredAttributes(serviceData *pb.ServiceData) (twirp.Error) {
	if serviceData.FullName == "" || serviceData.FirstName == "" || serviceData.LastName == "" || serviceData.Address.Data == nil || serviceData.ServiceDidId == "" || serviceData.Avatar == nil{ // serviceData.Artists?
		var argument string
		switch {
		case serviceData.FullName == "":
			argument = "full_name"
		case serviceData.FirstName == "":
			argument = "first_name"
		case serviceData.LastName == "":
			argument = "last_name"
		case serviceData.Address.Data == nil:
			argument = "address_data"
		case serviceData.ServiceDidId == "":
			argument = "persona_id"
		case serviceData.Avatar == nil:
			argument = "avatar"
		}
		return twirp.RequiredArgumentError(argument)
	}
	return nil
}
