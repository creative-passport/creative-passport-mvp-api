package servicedataserver_test

import (
	// "fmt"
	// "reflect"
	"context"

	"github.com/go-pg/pg"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/servicedata"
	"mycelia/internal/database/models"
)

var _ = Describe("ServiceData server", func() {
	const already_exists_code twirp.ErrorCode = "already_exists"
	const invalid_argument_code twirp.ErrorCode = "invalid_argument"
	const not_found_code twirp.ErrorCode = "not_found"

	Describe("GetServiceData", func() {
		Context("with valid uuid", func() {
			It("should respond with serviceData if it exists", func() {
				serviceData := &pb.ServiceData{Id: newServiceData.Id.String()}

				res, err := service.GetServiceData(context.Background(), serviceData)

				Expect(err).NotTo(HaveOccurred())
				Expect(res.ServiceDidId).To(Equal(newServiceData.ServiceDidId.String()))
				Expect(res.Biography).To(Equal(newServiceData.Biography))
				Expect(res.FullName).To(Equal(newServiceData.FullName))
				Expect(res.FirstName).To(Equal(newServiceData.FirstName))
				Expect(res.LastName).To(Equal(newServiceData.LastName))
				Expect(res.PhoneNumber).To(Equal(newServiceData.PhoneNumber))
				Expect(res.BusinessEmail).To(Equal(newServiceData.BusinessEmail))
				Expect(res.Address.Id).To(Equal(newServiceData.AddressId.String()))


			})
			It("should respond with not_found error if serviceData does not exist", func() {
				id := uuid.NewV4()
				for id == newServiceData.Id {
					id = uuid.NewV4()
				}
				serviceData := &pb.ServiceData{Id: id.String()}
				resp, err := service.GetServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceData := &pb.ServiceData{Id: id}
				resp, err := service.GetServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("UpdateServiceData", func() {
		Context("with valid uuid", func() {
			It("should update serviceData if it exists", func() {
				serviceData := &pb.ServiceData{
					Id: newServiceData.Id.String(),
					ServiceDidId: newServiceData.ServiceDidId.String(),
					Biography: newServiceData.Biography,
					FullName: newServiceData.FullName,
					FirstName: newServiceData.FirstName,
					LastName: newServiceData.LastName,
					Avatar: newServiceData.Avatar,
					BusinessEmail: newServiceData.BusinessEmail,
					PhoneNumber: newServiceData.PhoneNumber, 
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},

				}
				_, err := service.UpdateServiceData(context.Background(), serviceData)

				Expect(err).NotTo(HaveOccurred())

				t := new(models.ServiceData)
				err = db.Model(t).Where("id = ?", newServiceData.Id).Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(t.Biography).To(Equal(serviceData.Biography))
				Expect(t.FirstName).To(Equal(serviceData.FirstName))
				Expect(t.Avatar).To(Equal(serviceData.Avatar))
				Expect(t.FullName).To(Equal(serviceData.FullName))
				Expect(t.FullName).To(Equal(serviceData.FullName))
				Expect(t.ServiceDidId.String()).To(Equal(serviceData.ServiceDidId))
				Expect(t.BusinessEmail).To(Equal(serviceData.BusinessEmail))

				address := new(models.StreetAddress)
				err = db.Model(address).Where("id = ?", newServiceData.AddressId).Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(address.Data).To(Equal(map[string]string{"some": "new data"}))

			})
			It("should respond with not_found error if serviceData does not exist", func() {
				id := uuid.NewV4()
				for id == newServiceData.Id {
					id = uuid.NewV4()
				}
				avatar := make([]byte, 5)

				serviceData := &pb.ServiceData{
					Id: id.String(),
					Biography: "best serviceData ever",
					FirstName: "Amy",
					LastName: "Jones",
					Avatar: avatar,
					ServiceDidId: newServiceData.ServiceDidId.String(),
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				resp, err := service.UpdateServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				//twerr := err.(twirp.Error)
				//Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				avatar := make([]byte, 5)

				serviceData := &pb.ServiceData{
					Id: id,
					Biography: "best serviceData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Avatar: avatar,
					ServiceDidId: newServiceData.ServiceDidId.String(),
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				resp, err := service.UpdateServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	Describe("CreateServiceData", func() {
		Context("with all required attributes", func() {
			It("should create a new serviceData", func() {

				avatar := make([]byte, 5)

				serviceData := &pb.ServiceData{
					Biography: "best serviceData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Avatar: avatar,
					ServiceDidId: newServiceData.ServiceDidId.String(),
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				resp, err := service.CreateServiceData(context.Background(), serviceData)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp).NotTo(BeNil())

				Expect(resp.Id).NotTo(BeNil())
				Expect(resp.FirstName).To(Equal(serviceData.FirstName))
				Expect(resp.LastName).To(Equal(serviceData.LastName))
				Expect(resp.FullName).To(Equal(serviceData.FullName))
				Expect(resp.Avatar).To(Equal(serviceData.Avatar))
				Expect(resp.ServiceDidId).To(Equal(serviceData.ServiceDidId))

				did := new(models.ServiceDid)
				err = db.Model(did).Where("id = ?", serviceData.ServiceDidId).Column("service_did.*", "AssociatedData").Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(did.AssociatedData)).To(Equal(2))

				serviceDataId, err := uuid.FromString(resp.Id)
				Expect(err).NotTo(HaveOccurred())
				var arr []uuid.UUID
				for _, data := range did.AssociatedData{
					arr = append(arr, data.Id)
				}
				Expect(arr).To(ContainElement(serviceDataId))
			})
		})

		Context("with missing required attributes", func() {
			It("should not create a serviceData without ServiceDidId", func() {
				avatar := make([]byte, 5)
				serviceData := &pb.ServiceData{
					Biography: "best serviceData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Avatar: avatar,
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},
				}
				resp, err := service.CreateServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())
				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("persona_id"))
			})
			It("should not create a serviceData without avatar", func() {
				serviceData := &pb.ServiceData{
					Biography: "best serviceData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},
					ServiceDidId: newServiceData.ServiceDidId.String(),
				}
				resp, err := service.CreateServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("avatar"))
			})
			
		})

		Context("with invalid attributes", func() {
			It("should not create a serviceData if ServiceDidId is invalid", func() {
				avatar := make([]byte, 5)
				serviceData := &pb.ServiceData{
					Biography: "best serviceData ever",
					FirstName: "Amy",
					LastName: "Jones",
					FullName: "AmyJones",
					Avatar: avatar,
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},
					ServiceDidId: "1111",
				}
				resp, err := service.CreateServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())
				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
			})
			It("should not create a serviceData if ServiceDidId does not exist", func() {
				avatar := make([]byte, 5)

				userId := uuid.NewV4()
				for userId == newServiceData.Id {
					userId = uuid.NewV4()
				}
				serviceData := &pb.ServiceData{
					Biography: "best serviceData ever",
					FirstName: "Amy",
					LastName: "Jones",
					Avatar: avatar,
					FullName: "AmyJones",
					Address: &pb.ServiceStreetAddress{Id: newServiceData.AddressId.String(), Data: map[string]string{"some": "new data"}},
					ServiceDidId: userId.String(),
				}
				resp, err := service.CreateServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())
				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})

		})
	})

	Describe("DeleteServiceData", func() {
		Context("with valid uuid", func() {
			It("should delete serviceData if it exists", func() {
				serviceData := &pb.ServiceData{Id: newServiceData.Id.String()}

				serviceDataToDelete := new(models.ServiceData)
				err := db.Model(serviceDataToDelete).Where("id = ?", newServiceData.Id).Select()
				Expect(err).NotTo(HaveOccurred())

				_, err = service.DeleteServiceData(context.Background(), serviceData)
				Expect(err).NotTo(HaveOccurred())


				var serviceDatas []models.ServiceData
				err = db.Model(&serviceDatas).
					Where("id in (?)", pg.In([]uuid.UUID{serviceDataToDelete.Id})).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(serviceDatas)).To(Equal(0))
			})
			It("should respond with not_found error if serviceData does not exist", func() {
				id := uuid.NewV4()
				for id == newServiceData.Id {
					id = uuid.NewV4()
				}
				serviceData := &pb.ServiceData{Id: id.String()}
				resp, err := service.DeleteServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceData := &pb.ServiceData{Id: id}
				resp, err := service.DeleteServiceData(context.Background(), serviceData)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})
})
