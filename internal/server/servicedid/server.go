package servicedidserver

import (
	"fmt"
	// "reflect"
	"time"
	"context"
	//"net/url"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/servicedid"
	datapb "mycelia/rpc/servicedata"
	userdatapb "mycelia/rpc/personadata"
	//datapb "mycelia/rpc/personadata"
	"mycelia/internal"
	"mycelia/internal/database/models"
)

type Server struct {
	db *pg.DB
}

func NewServer(db *pg.DB) *Server {
	return &Server{db: db}
}

func (s *Server) CreateServiceDid(ctx context.Context, serviceDid *pb.ServiceDid) (*pb.ServiceDid, error) {
	createServiceDid := func(serviceDid *pb.ServiceDid, serviceId uuid.UUID) (error, string, *models.ServiceDid) {
		var table string
		tx, err := s.db.Begin()
		if err != nil {
			return err, table, nil
		}
		defer tx.Rollback()

		activeUser := true 

		newServiceDid := &models.ServiceDid{
			Did: serviceDid.Did,
			PublicKey: serviceDid.PublicKey,
			PrivateKey: serviceDid.PrivateKey,
			EthereumAddress: serviceDid.EthereumAddress,
			Notes: serviceDid.Notes,
			Active: activeUser,
			ServiceId: serviceId,
			DidType: serviceDid.DidType,
		}
		_, pgerr := tx.Model(newServiceDid).Returning("*").Insert()
		if pgerr != nil {
			return pgerr, "service_did", nil
		}


		return tx.Commit(), table, newServiceDid
	}

	requiredErr := checkRequiredAttributes(serviceDid)
	if requiredErr != nil {
		return nil, requiredErr
	}

	ownerId, err := internal.GetUuidFromString(serviceDid.ServiceId)
	if err != nil {
		return nil, err
	}

	pgerr, table, newServiceDid := createServiceDid(serviceDid, ownerId)
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, table)
	}

  return &pb.ServiceDid{
		Id: newServiceDid.Id.String(),
		PublicKey: newServiceDid.PublicKey,
		PrivateKey: newServiceDid.PrivateKey,
		EthereumAddress: newServiceDid.EthereumAddress,
		Notes: newServiceDid.Notes, 
		ServiceId: newServiceDid.ServiceId.String(),
		Active: newServiceDid.Active,
		DidType: newServiceDid.DidType,

	}, nil
}

// TODO handle privacy settings
func (s *Server) GetServiceDid(ctx context.Context, serviceDid *pb.ServiceDid) (*pb.ServiceDid, error) {
	id, err := internal.GetUuidFromString(serviceDid.Id)
	if err != nil {
		return nil, err
	}
	u := &models.ServiceDid{Id: id}

	pgerr := s.db.Model(u).
		Column("service_did.*", "AssociatedData").
		WherePK().
		Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "service_did")
	}


	return &pb.ServiceDid{
		Id: u.Id.String(),
		Did: u.Did,
		PrivateKey: u.PrivateKey,
		PublicKey: u.PublicKey,
		EthereumAddress: u.EthereumAddress,
		Notes: u.Notes,
		Active: u.Active,
		DidType: u.DidType,
		ServiceId: u.ServiceId.String(),
		AssociatedData: getServiceDataResponse(u.AssociatedData),
		//Claims: claims, 
	}, nil
}

func (s *Server) UpdateServiceDid(ctx context.Context, serviceDid *pb.ServiceDid) (*userdatapb.Empty, error) {
	updateServiceDid := func(serviceDid *pb.ServiceDid, u *models.ServiceDid) (error, string) {
		var table string
		tx, err := s.db.Begin()
		if err != nil {
			return err, "service_did"
		}
		defer tx.Rollback()


		u.UpdatedAt = time.Now()
		_, err = tx.Model(u).WherePK().Returning("*").UpdateNotNull()
		if err != nil {
			return err, "service_did"
		}

		return tx.Commit(), table
	}

	rerr := checkRequiredAttributes(serviceDid)
	if rerr != nil {
		return nil, rerr
	}

	u, err := getServiceDidModel(serviceDid)
	if err != nil {
		return nil, err
	}

	pgerr, _ := updateServiceDid(serviceDid, u)
	twerr := internal.CheckError(pgerr, "service_did")
	if twerr != nil {
		return nil, twerr
	}

  return &userdatapb.Empty{}, nil
}

func (s *Server) DeleteServiceDid(ctx context.Context, serviceDid *pb.ServiceDid) (*userdatapb.Empty, error) {
	id, twerr := internal.GetUuidFromString(serviceDid.Id)
	if twerr != nil {
		return nil, twerr
	}
	u := &models.ServiceDid{Id: id}

	tx, err := s.db.Begin()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}
	defer tx.Rollback()

	//delete claims
	var cs []models.Claim

	cerr := tx.Model(&cs).
    WhereGroup(func(q *orm.Query) (*orm.Query, error) {
        q = q.WhereOr("issuing_service_id = ?", id).
            WhereOr("subject_service_id = ?", id)
	return q, nil
    }).
    Select()

	if cerr != nil{
		return nil, cerr
	}

	for _, claim := range cs{
		fmt.Printf("Deleting Claim:")
		fmt.Printf(claim.Id.String())
		err, _ = claim.Delete(tx)
		if err != nil{
			return nil, err
		}
	}

	pgerr := s.db.Model(u).
		Column("service_did.*", "AssociatedData").
		WherePK().
		Select()
	if pgerr != nil {
		return nil, internal.CheckError(pgerr, "service_did")
	}
	
	for _, data := range u.AssociatedData{
		fmt.Printf("Deleting Data:")
		fmt.Printf(data.Id.String())
		derr, _ := data.Delete(tx)
		if derr != nil{
			return nil, derr
		}
	}


	if pgerr, table := u.Delete(tx); pgerr != nil {
		return nil, internal.CheckError(pgerr, table)
	}

	err = tx.Commit()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}

	return &userdatapb.Empty{}, nil
}

func (s *Server) IssueServiceClaim(ctx context.Context, claim *userdatapb.Claim)(*userdatapb.Claim, error){
	var newClaim *models.Claim
	err, cl := newClaim.Create(s.db, claim)
	if err != nil{
		return nil, err
	}
	return cl, nil

}

func (s *Server) UpdateServiceClaim(ctx context.Context, claim *userdatapb.Claim)(*userdatapb.Empty, error){
	id, err := internal.GetUuidFromString(claim.Id)
	if err != nil{
		return nil, err
	}
	var claimModel *models.Claim
	claimModel.Id = id
	claimModel.Claim = claim.Claim
	claimModel.ClaimType = claim.ClaimType
	claimModel.Valid = claim.Valid
	claimModel.SocialGraphPriv = claim.SocialGraphPriv
	clerr, _ := claimModel.Update(s.db)
	if clerr != nil{
		return nil, clerr
	}

	return &userdatapb.Empty{}, nil

}

func (s *Server) DeleteServiceClaim(ctx context.Context, claim *userdatapb.Claim)(*userdatapb.Empty, error){
	id, ierr := internal.GetUuidFromString(claim.Id)
	if ierr != nil{
		return nil, ierr
	}
	tx, err := s.db.Begin()
	if err != nil {
		return nil, internal.CheckError(err, "")
	}
	defer tx.Rollback()

	//delete claims
	var cs *models.Claim

	cerr := tx.Model(&cs).
	Where("id = ?", id).
    Select()

	if cerr != nil{
		return nil, cerr
	}
	
	fmt.Printf("Deleting Claim:")
	fmt.Printf(cs.Id.String())
	err, _ = cs.Delete(tx)
	if err != nil{
		return nil, err
	}
	
	return &userdatapb.Empty{}, nil



}


func getServiceDidModel(serviceDid *pb.ServiceDid) (*models.ServiceDid, twirp.Error) {
	id, err := internal.GetUuidFromString(serviceDid.Id)
	if err != nil {
		return nil, err
	}
	
	serviceId, err := internal.GetUuidFromString(serviceDid.ServiceId)
	if err != nil {
		return nil, err
	}

	return &models.ServiceDid{
		Id: id,
		Did: serviceDid.Did,
		PrivateKey: serviceDid.PrivateKey,
		PublicKey: serviceDid.PublicKey,
		EthereumAddress: serviceDid.EthereumAddress,
		Active: serviceDid.Active,
		DidType: serviceDid.DidType,
		ServiceId: serviceId,
	}, nil
}


func getServiceDataIds(l []*datapb.ServiceData, db *pg.Tx) ([]uuid.UUID, error) {
	serviceData := make([]*models.ServiceData, len(l))
	serviceDataIds := make([]uuid.UUID, len(l))
	for i, data := range l {
		if data.Id == "" {
			serviceData[i] = &models.ServiceData{FullName: data.FullName, PhoneNumber: data.PhoneNumber}
			_, pgerr := db.Model(serviceData[i]).Returning("*").Insert()
			if pgerr != nil {
				return nil, pgerr
			}
			serviceDataIds[i] = serviceData[i].Id
			data.Id = serviceData[i].Id.String()
		} else {
			serviceDataId, twerr := internal.GetUuidFromString(data.Id)
			if twerr != nil {
				return nil, twerr.(error)
			}
			serviceDataIds[i] = serviceDataId
		}
	}
	return serviceDataIds, nil
}

func checkRequiredAttributes(serviceDid *pb.ServiceDid) (twirp.Error) {
	if serviceDid.PublicKey == "" || serviceDid.Did == "" || serviceDid.PrivateKey == "" || serviceDid.EthereumAddress == "" || serviceDid.ServiceId == "" {
		var argument string
		switch {
		case serviceDid.PublicKey == "":
			argument = "public_key"
		case serviceDid.Did == "":
			argument = "did"
		case serviceDid.PrivateKey == "":
			argument = "private_key"
		case serviceDid.EthereumAddress == "":
			argument = "ethereum_address"
		case serviceDid.ServiceId == "":
			argument = "service_id"

		}
		return twirp.RequiredArgumentError(argument)
	}
	return nil
}

func getServiceDataResponse(associatedData []models.ServiceData) ([]*datapb.ServiceData) {
	datas := make([]*datapb.ServiceData, len(associatedData))
	for i, data := range associatedData {
		datas[i] = &datapb.ServiceData{Id: data.Id.String(), FullName: data.FullName, Avatar: data.Avatar}
	}
	return datas
}
