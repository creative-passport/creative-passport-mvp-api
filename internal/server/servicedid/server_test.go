package servicedidserver_test

import (
	// "fmt"
	// "reflect"
	"context"

	"github.com/go-pg/pg"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/twitchtv/twirp"
	"github.com/satori/go.uuid"

	pb "mycelia/rpc/servicedid"
	"mycelia/internal/database/models"
)

var _ = Describe("ServiceDid server", func() {
  const already_exists_code twirp.ErrorCode = "already_exists"
  const invalid_argument_code twirp.ErrorCode = "invalid_argument"
  const not_found_code twirp.ErrorCode = "not_found"

	Describe("GetServiceDid", func() {
		Context("with valid uuid", func() {
			It("should respond with service_did if it exists", func() {
				serviceDid := &pb.ServiceDid{Id: newServiceDid.Id.String()}
				resp, err := service.GetServiceDid(context.Background(), serviceDid)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Id).To(Equal(newServiceDid.Id.String()))
				Expect(resp.Did).To(Equal(newServiceDid.Did))
				Expect(resp.PrivateKey).To(Equal(newServiceDid.PrivateKey))
				Expect(resp.PublicKey).To(Equal(newServiceDid.PublicKey))
				Expect(resp.EthereumAddress).To(Equal(newServiceDid.EthereumAddress))
				Expect(resp.Active).To(Equal(newServiceDid.Active))
				Expect(resp.DidType).To(Equal(newServiceDid.DidType))
				Expect(resp.ServiceId).To(Equal(newServiceDid.ServiceId.String()))


				Expect(len(resp.AssociatedData)).To(Equal(1))
				Expect(resp.AssociatedData[0].Id).To(Equal(newServiceData.Id.String()))
				Expect(resp.AssociatedData[0].FullName).To(Equal(newServiceData.FullName))
			})
			It("should respond with not_found error if service_did does not exist", func() {
				id := uuid.NewV4()
				for (id == newServiceDid.Id ) {
					id = uuid.NewV4()
				}
				serviceDid := &pb.ServiceDid{Id: id.String()}
				resp, err := service.GetServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceDid := &pb.ServiceDid{Id: id}
				resp, err := service.GetServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})


	Describe("UpdateServiceDid", func() {
		Context("with valid uuid", func() {
			It("should update service_did if it exists", func() {
				serviceDid := &pb.ServiceDid{
					Id: newServiceDid.Id.String(),
					Did: "newdid",
					PrivateKey: "newprivatekey",
					PublicKey: "newpublickey",
					DidType: "service",
					EthereumAddress: newServiceDid.EthereumAddress,
					ServiceId: newServiceDid.ServiceId.String(),
				}
				_, err := service.UpdateServiceDid(context.Background(), serviceDid)

				Expect(err).NotTo(HaveOccurred())



				updatedServiceDid := new(models.ServiceDid)
				err = db.Model(updatedServiceDid).Where("id = ?", newServiceDid.Id).Select()
				Expect(err).NotTo(HaveOccurred())

			})
			It("should respond with not_found error if service_did does not exist", func() {
				id := uuid.NewV4()
				for (id == newServiceDid.Id ) {
					id = uuid.NewV4()
				}
				serviceDid := &pb.ServiceDid{
					Id: id.String(),
					Did: newServiceDid.Did,
					PrivateKey: newServiceDid.PrivateKey,
					PublicKey: newServiceDid.PublicKey,
					DidType: newServiceDid.EthereumAddress,
					EthereumAddress: newServiceDid.EthereumAddress,
					ServiceId: newServiceDid.ServiceId.String(),
				}
				resp, err := service.UpdateServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				//twerr := err.(twirp.Error)
				//Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceDid := &pb.ServiceDid{
					Id: id,
					Did: newServiceDid.Did,
					PrivateKey: newServiceDid.PrivateKey,
					PublicKey: newServiceDid.PublicKey,
					DidType: newServiceDid.EthereumAddress,
					EthereumAddress: newServiceDid.EthereumAddress,
					ServiceId: newServiceDid.ServiceId.String(),
				}
				resp, err := service.UpdateServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

	

	Describe("DeleteServiceDid", func() {
		Context("with valid uuid", func() {
			It("should delete service_did if it exists", func() {
				serviceDid := &pb.ServiceDid{Id: newServiceDid.Id.String()}

				serviceDidToDelete := new(models.ServiceDid)
				err := db.Model(serviceDidToDelete).
					Column("AssociatedData", "service_did.*").
					Where("id = ?", newServiceDid.Id).Select()
				Expect(err).NotTo(HaveOccurred())


				_, err = service.DeleteServiceDid(context.Background(), serviceDid)
				Expect(err).NotTo(HaveOccurred())




				var associatedData []models.ServiceData
				err = db.Model(&associatedData).
					Where("service_did_id = ?", newServiceDid.Id).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(associatedData)).To(Equal(0))


				var serviceDids []models.ServiceDid
				err = db.Model(&serviceDids).
					Where("id in (?)", pg.In([]uuid.UUID{serviceDidToDelete.Id})).
					Select()
				Expect(err).NotTo(HaveOccurred())
				Expect(len(serviceDids)).To(Equal(0))
			})
			It("should respond with not_found error if service_did does not exist", func() {
				id := uuid.NewV4()
				for (id == newServiceDid.Id) {
					id = uuid.NewV4()
				}
				serviceDid := &pb.ServiceDid{Id: id.String()}
				resp, err := service.DeleteServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(not_found_code))
			})
		})
		Context("with invalid uuid", func() {
			It("should respond with invalid_argument error", func() {
				id := "45"
				serviceDid := &pb.ServiceDid{Id: id}
				resp, err := service.DeleteServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("id"))
			})
		})
	})

  Describe("CreateServiceDid", func() {
		Context("with all required attributes", func() {
			It("should create a new service_did", func() {
	
				serviceId := newService.Id.String()

				serviceDid := &pb.ServiceDid{
					Did: "newdid",
					PrivateKey: "newprivatekey",
					PublicKey: "newpublickey",
					DidType: "service",
					EthereumAddress: "newethereumaddress",
					ServiceId: serviceId,
				}
				resp, err := service.CreateServiceDid(context.Background(), serviceDid)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Id).NotTo(Equal(""))
				Expect(resp.PrivateKey).To(Equal("newprivatekey"))
				Expect(resp.PublicKey).To(Equal("newpublickey"))
				Expect(resp.EthereumAddress).To(Equal("newethereumaddress"))
				Expect(resp.DidType).To(Equal("service"))
				Expect(resp.ServiceId).To(Equal(serviceId))

				id, err := uuid.FromString(resp.Id)
				Expect(err).NotTo(HaveOccurred())
				updatedServiceDid := new(models.ServiceDid)
				err = db.Model(updatedServiceDid).Where("id = ?", id).Select()
				Expect(err).NotTo(HaveOccurred())

			})

			It("should not create a service_did with same did", func() {
				serviceId := newService.Id.String()
				serviceDidId := newServiceDid.Id.String()
				serviceDid := &pb.ServiceDid{
					Id: serviceDidId,
					Did: "newdid",
					PrivateKey: "secondprivkey",
					PublicKey: "secondpubkey",
					DidType: "persona",
					EthereumAddress: "secondethadd",
					ServiceId: serviceId,
				}
				resp, err := service.CreateServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(already_exists_code))
				Expect(twerr.Msg()).To(Equal("did"))
			})
		})

		Context("with missing required attributes", func() {
			It("should not create a service_did without did", func() {
				serviceId := newService.Id.String()
				serviceDidId := newServiceDid.Id.String()
				serviceDid := &pb.ServiceDid{
					PrivateKey: "secondprivkey",
					PublicKey: "secondpubkey",
					DidType: "persona",
					EthereumAddress: "secondethadd",
					ServiceId: serviceId,
					Id: serviceDidId,
				}
				resp, err := service.CreateServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("did"))

			})


		})
  })
  Describe("CreateSocialGraphConnection", func() {
		Context("with all required attributes", func() {
			It("should create a new social graph connection", func() {
	
				serviceId := newService.Id.String()

				serviceDid := &pb.ServiceDid{
					Did: "newdid",
					PrivateKey: "newprivatekey",
					PublicKey: "newpublickey",
					DidType: "service",
					EthereumAddress: "newethereumaddress",
					ServiceId: serviceId,
				}
				resp, err := service.CreateServiceDid(context.Background(), serviceDid)

				Expect(err).NotTo(HaveOccurred())
				Expect(resp.Id).NotTo(Equal(""))
				Expect(resp.PrivateKey).To(Equal("newprivatekey"))
				Expect(resp.PublicKey).To(Equal("newpublickey"))
				Expect(resp.EthereumAddress).To(Equal("newethereumaddress"))
				Expect(resp.DidType).To(Equal("service"))
				Expect(resp.ServiceId).To(Equal(serviceId))

				id, err := uuid.FromString(resp.Id)
				Expect(err).NotTo(HaveOccurred())
				updatedServiceDid := new(models.ServiceDid)
				err = db.Model(updatedServiceDid).Where("id = ?", id).Select()
				Expect(err).NotTo(HaveOccurred())

			})

			It("should not create a service_did with same did", func() {
				serviceId := newService.Id.String()
				serviceDidId := newServiceDid.Id.String()
				serviceDid := &pb.ServiceDid{
					Id: serviceDidId,
					Did: "newdid",
					PrivateKey: "secondprivkey",
					PublicKey: "secondpubkey",
					DidType: "persona",
					EthereumAddress: "secondethadd",
					ServiceId: serviceId,
				}
				resp, err := service.CreateServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(already_exists_code))
				Expect(twerr.Msg()).To(Equal("did"))
			})
		})

		Context("with missing required attributes", func() {
			It("should not create a service_did without did", func() {
				serviceId := newService.Id.String()
				serviceDidId := newServiceDid.Id.String()
				serviceDid := &pb.ServiceDid{
					PrivateKey: "secondprivkey",
					PublicKey: "secondpubkey",
					DidType: "persona",
					EthereumAddress: "secondethadd",
					ServiceId: serviceId,
					Id: serviceDidId,
				}
				resp, err := service.CreateServiceDid(context.Background(), serviceDid)

				Expect(resp).To(BeNil())
				Expect(err).To(HaveOccurred())

				twerr := err.(twirp.Error)
				Expect(twerr.Code()).To(Equal(invalid_argument_code))
				Expect(twerr.Meta("argument")).To(Equal("did"))

			})


		})
  })

})
