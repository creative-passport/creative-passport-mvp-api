package servicedidserver_test

import (
	"testing"
	// "fmt"

	// pb "mycelia/rpc/user"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/go-pg/pg"

	"mycelia/internal/database"
	"mycelia/internal/database/models"
	servicedidserver "mycelia/internal/server/servicedid"
)

var (
	db *pg.DB
	service *servicedidserver.Server
	newService *models.Service
	newServiceDid *models.ServiceDid
	newServiceData *models.ServiceData
	newAddress *models.StreetAddress
)

func TestServicegroup(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "ServiceDid server Suite")
}

var _ = BeforeSuite(func() {
		testing := true
		db = database.Connect(testing)
		service = servicedidserver.NewServer(db)


		
		//Create a new service

		newService = &models.Service{Username: "service name", Email: "service@service.com"}
		err := db.Insert(newService)
		Expect(err).NotTo(HaveOccurred())


		newAddress = &models.StreetAddress{Data: map[string]string{"some": "data"}}
		err = db.Insert(newAddress)
		Expect(err).NotTo(HaveOccurred())


		newServiceDid = &models.ServiceDid{
			ServiceId: newService.Id,
			Did: "DidString",
			PrivateKey: "PrivKeyString",
			PublicKey: "PubKeyString",
			EthereumAddress: "EthAddress",
			DidType: "service",
		}
		_, err = db.Model(newServiceDid).Returning("*").Insert()
		Expect(err).NotTo(HaveOccurred())

		avatar := make([]byte, 5)

		newServiceData = &models.ServiceData{
		    ServiceDidId: newServiceDid.Id,
		    Biography: "short bio",
		    FullName: "TODO this should just concat first and last names",
		    FirstName: "Jenn",
		    LastName: "Fain",
			PhoneNumber: "01718415878",
			Avatar: avatar,
			AddressId: newAddress.Id,
		}
		_, err = db.Model(newServiceData).Returning("*").Insert()
		Expect(err).NotTo(HaveOccurred())


})

var _ = AfterSuite(func() {

	// delete all claims
	var claims []models.Claim
	err := db.Model(&claims).Select()
	if len(claims) > 0{
		_, err = db.Model(&claims).Delete()
		Expect(err).NotTo(HaveOccurred())
	}

	// Delete all streetAddresses
	var streetAddresses []models.StreetAddress
	err = db.Model(&streetAddresses).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(streetAddresses) > 0 {
		_, err = db.Model(&streetAddresses).Delete()
		Expect(err).NotTo(HaveOccurred())
	}


	//delete all serviceData
	var serviceData [] models.ServiceData
	err = db.Model(&serviceData).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(serviceData) > 0{
		_, err = db.Model(&serviceData).Delete()
		Expect(err).NotTo(HaveOccurred())
	}



	// Delete all serviceDids
	var serviceDids []models.ServiceDid
	err = db.Model(&serviceDids).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(serviceDids) > 0 {
		_, err = db.Model(&serviceDids).Delete()
		Expect(err).NotTo(HaveOccurred())
	}



	// Delete all services
	var services []models.Service
	err = db.Model(&services).Select()
	Expect(err).NotTo(HaveOccurred())
	if len(services) >0{
		_, err = db.Model(&services).Delete()
		Expect(err).NotTo(HaveOccurred())
	}



})

