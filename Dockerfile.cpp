FROM golang:1.10

RUN mkdir -p /go/src/mycelia
WORKDIR /go/src/mycelia

ADD . /go/src/mycelia