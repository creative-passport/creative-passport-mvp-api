package config

import (
	"fmt"
    "github.com/joho/godotenv"
)



var Config *config

func init() {
	var myEnv map[string]string
	myEnv, err := godotenv.Read()
	if err != nil{
		fmt.Printf(".env file did not initiate")
	}

	addr := myEnv["POSTGRES_ADDR"] + ":" + myEnv["POSTGRES_PORT"]
	DevConfig := db_config{addr, "cpp_user", "password", "cpp_dev"}
	TestingConfig := db_config{addr, "cpp_testing_user", "password", "cpp_testing"}
	Config = &config{TestingConfig, DevConfig}
}

type db_config struct {
  Addr         string `json:"addr"`
  User     string `json:"user"`
  Password     string `json:"password"`
  Database string `json:"database"`
}

type config struct {
  Testing db_config `json:"testing"`
	Dev     db_config `json:"dev"`
}
