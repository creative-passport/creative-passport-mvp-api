// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rpc/servicedid/service.proto

package servicedid

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "mycelia/rpc/personadata"
import servicedata "mycelia/rpc/servicedata"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type ServiceDid struct {
	Id                   string                     `protobuf:"bytes,1,opt,name=id" json:"id,omitempty"`
	PrivateKey           string                     `protobuf:"bytes,2,opt,name=private_key,json=privateKey" json:"private_key,omitempty"`
	PublicKey            string                     `protobuf:"bytes,3,opt,name=public_key,json=publicKey" json:"public_key,omitempty"`
	Did                  string                     `protobuf:"bytes,4,opt,name=did" json:"did,omitempty"`
	EthereumAddress      string                     `protobuf:"bytes,5,opt,name=ethereum_address,json=ethereumAddress" json:"ethereum_address,omitempty"`
	Notes                string                     `protobuf:"bytes,6,opt,name=notes" json:"notes,omitempty"`
	ServiceId            string                     `protobuf:"bytes,7,opt,name=service_id,json=serviceId" json:"service_id,omitempty"`
	AssociatedData       []*servicedata.ServiceData `protobuf:"bytes,8,rep,name=associated_data,json=associatedData" json:"associated_data,omitempty"`
	Active               bool                       `protobuf:"varint,9,opt,name=active" json:"active,omitempty"`
	DidType              string                     `protobuf:"bytes,10,opt,name=did_type,json=didType" json:"did_type,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                   `json:"-"`
	XXX_unrecognized     []byte                     `json:"-"`
	XXX_sizecache        int32                      `json:"-"`
}

func (m *ServiceDid) Reset()         { *m = ServiceDid{} }
func (m *ServiceDid) String() string { return proto.CompactTextString(m) }
func (*ServiceDid) ProtoMessage()    {}
func (*ServiceDid) Descriptor() ([]byte, []int) {
	return fileDescriptor_service_805cb5a2fab0ce86, []int{0}
}
func (m *ServiceDid) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ServiceDid.Unmarshal(m, b)
}
func (m *ServiceDid) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ServiceDid.Marshal(b, m, deterministic)
}
func (dst *ServiceDid) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ServiceDid.Merge(dst, src)
}
func (m *ServiceDid) XXX_Size() int {
	return xxx_messageInfo_ServiceDid.Size(m)
}
func (m *ServiceDid) XXX_DiscardUnknown() {
	xxx_messageInfo_ServiceDid.DiscardUnknown(m)
}

var xxx_messageInfo_ServiceDid proto.InternalMessageInfo

func (m *ServiceDid) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *ServiceDid) GetPrivateKey() string {
	if m != nil {
		return m.PrivateKey
	}
	return ""
}

func (m *ServiceDid) GetPublicKey() string {
	if m != nil {
		return m.PublicKey
	}
	return ""
}

func (m *ServiceDid) GetDid() string {
	if m != nil {
		return m.Did
	}
	return ""
}

func (m *ServiceDid) GetEthereumAddress() string {
	if m != nil {
		return m.EthereumAddress
	}
	return ""
}

func (m *ServiceDid) GetNotes() string {
	if m != nil {
		return m.Notes
	}
	return ""
}

func (m *ServiceDid) GetServiceId() string {
	if m != nil {
		return m.ServiceId
	}
	return ""
}

func (m *ServiceDid) GetAssociatedData() []*servicedata.ServiceData {
	if m != nil {
		return m.AssociatedData
	}
	return nil
}

func (m *ServiceDid) GetActive() bool {
	if m != nil {
		return m.Active
	}
	return false
}

func (m *ServiceDid) GetDidType() string {
	if m != nil {
		return m.DidType
	}
	return ""
}

func init() {
	proto.RegisterType((*ServiceDid)(nil), "mycelia.api.creativepassport.ServiceDid")
}

func init() {
	proto.RegisterFile("rpc/servicedid/service.proto", fileDescriptor_service_805cb5a2fab0ce86)
}

var fileDescriptor_service_805cb5a2fab0ce86 = []byte{
	// 410 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x54, 0xd1, 0x6a, 0x53, 0x41,
	0x10, 0x25, 0x89, 0x49, 0x93, 0xa9, 0xb6, 0xe9, 0x22, 0xb2, 0x86, 0x8a, 0x41, 0x11, 0xd2, 0x97,
	0x5b, 0xa8, 0x5f, 0xa0, 0xad, 0x48, 0xf1, 0x2d, 0xea, 0x8b, 0x60, 0x2f, 0xd3, 0x9d, 0x11, 0x07,
	0x93, 0xde, 0x65, 0x77, 0x13, 0xb8, 0x1f, 0xe8, 0x27, 0xf8, 0x3f, 0xb2, 0x7b, 0xef, 0x25, 0xbd,
	0x08, 0x35, 0x94, 0xe6, 0x6d, 0xe6, 0x9c, 0xb3, 0x73, 0x66, 0xce, 0xc3, 0xc2, 0xb1, 0xb3, 0xe6,
	0xd4, 0xb3, 0x5b, 0x8b, 0x61, 0x12, 0x6a, 0xca, 0xcc, 0xba, 0x22, 0x14, 0xea, 0x78, 0x59, 0x1a,
	0x5e, 0x08, 0x66, 0x68, 0x25, 0x33, 0x8e, 0x31, 0xc8, 0x9a, 0x2d, 0x7a, 0x6f, 0x0b, 0x17, 0x26,
	0x6f, 0x6a, 0xf6, 0xf4, 0xf6, 0x0c, 0x0c, 0xd8, 0x1e, 0xd2, 0x96, 0x59, 0x76, 0xbe, 0xb8, 0xc1,
	0x7f, 0x65, 0xaf, 0xfe, 0x74, 0x01, 0x3e, 0x57, 0xc8, 0x85, 0x90, 0x3a, 0x80, 0xae, 0x90, 0xee,
	0x4c, 0x3b, 0xb3, 0xd1, 0xbc, 0x2b, 0xa4, 0x5e, 0xc2, 0xbe, 0x75, 0xb2, 0xc6, 0xc0, 0xf9, 0x2f,
	0x2e, 0x75, 0x37, 0x11, 0x50, 0x43, 0x9f, 0xb8, 0x54, 0x2f, 0x00, 0xec, 0xea, 0x7a, 0x21, 0x26,
	0xf1, 0xbd, 0xc4, 0x8f, 0x2a, 0x24, 0xd2, 0x63, 0xe8, 0x91, 0x90, 0x7e, 0x94, 0xf0, 0x58, 0xaa,
	0x13, 0x18, 0x73, 0xf8, 0xc9, 0x8e, 0x57, 0xcb, 0x1c, 0x89, 0x1c, 0x7b, 0xaf, 0xfb, 0x89, 0x3e,
	0x6c, 0xf0, 0x77, 0x15, 0xac, 0x9e, 0x42, 0xff, 0xa6, 0x08, 0xec, 0xf5, 0x20, 0xf1, 0x55, 0x13,
	0x1d, 0xeb, 0x13, 0x72, 0x21, 0xbd, 0x57, 0x39, 0xd6, 0xc8, 0x25, 0xa9, 0x39, 0x1c, 0xa2, 0xf7,
	0x85, 0x11, 0x0c, 0x4c, 0x79, 0xbc, 0x58, 0x0f, 0xa7, 0xbd, 0xd9, 0xfe, 0xd9, 0x49, 0x76, 0x57,
	0xac, 0x59, 0x13, 0x02, 0x06, 0x9c, 0x1f, 0x6c, 0x26, 0xc4, 0x5e, 0x3d, 0x83, 0x01, 0x9a, 0xa8,
	0xd6, 0xa3, 0x69, 0x67, 0x36, 0x9c, 0xd7, 0x9d, 0x7a, 0x0e, 0x43, 0x12, 0xca, 0x43, 0x69, 0x59,
	0x43, 0x5a, 0x64, 0x8f, 0x84, 0xbe, 0x94, 0x96, 0xcf, 0x7e, 0xf7, 0xe1, 0x68, 0x93, 0x6b, 0x5d,
	0xa9, 0x1f, 0x30, 0x3e, 0x8f, 0xc6, 0x7c, 0x2b, 0xf2, 0xd9, 0x76, 0x7b, 0x09, 0x4d, 0xb6, 0x56,
	0x2a, 0x03, 0x4f, 0x3e, 0x72, 0xd8, 0xb1, 0x09, 0xc2, 0xf8, 0xab, 0xa5, 0xfb, 0x1e, 0xf3, 0xfa,
	0x6e, 0xe5, 0x87, 0xa5, 0x0d, 0x65, 0xb4, 0xb8, 0xe0, 0x05, 0xef, 0xd2, 0xe2, 0x3b, 0x1c, 0x5d,
	0x7a, 0xbf, 0x6a, 0x1c, 0xce, 0x17, 0x28, 0x4b, 0xf5, 0x9f, 0x97, 0x49, 0x34, 0xd9, 0x46, 0xa4,
	0xae, 0x40, 0xb5, 0x42, 0x7a, 0xb8, 0xf9, 0xd5, 0xfa, 0x57, 0xa0, 0x5a, 0x09, 0x3d, 0xf0, 0xfc,
	0xf7, 0x8f, 0xbf, 0xc1, 0xe6, 0x9f, 0xba, 0x1e, 0xa4, 0x4f, 0xe3, 0xed, 0xdf, 0x00, 0x00, 0x00,
	0xff, 0xff, 0xc8, 0x71, 0x9f, 0xe2, 0xc0, 0x04, 0x00, 0x00,
}
