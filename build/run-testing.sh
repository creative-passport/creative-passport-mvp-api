# run migrations
go run internal/database/migrations/*.go testing

# run server
go run cmd/server/main.go