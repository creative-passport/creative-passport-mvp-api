FORMAT: 1A

# Claims in the Mycelia Creative Passport 

## Claims illustrated

For a rough understandng of how claims will work in the Creative Passport Alpha and Beta, please see this [whimsical diagram](https://whimsical.co/M76KhR8hkHNktgDowaCEcL).


## Centralisation of the Creative Passport Alpha 

The alpha of the Creative Passport is fully centralised. We have made this deceision in part because of time frames around the projected alpha launch (early September, before LifeId was ready to integrate) and because of a desire to get some user feedback before wading into blockchain-related design decisions (mostly around key management and claim publishing/revocation).

### Claims in the Creative Passport Alpha

Claims in the alpha are managed via `internal/database/models/claim.go`. 

At the moment, Claims are created passively: upon social graph connection initiation, upon persona creation, and upon persona_data creation. At the moment, claims are merely collected in the claims table, where they represent these connections between service_did/persona (membership claim), persona/persona/service_did (social graph claim), and persona/persona_data (data claim). 

The "claim" field in the Claim struct, where the hstore of claim data goes, is currently empty and needs to be implemented. When the Creative Passport is decentralised, these claim object will be transformed en masse into proper claims, if users choose to tranisition their data from the alpha to the beta. 

For the alpha release of the Creative Passport, we will implement an hstore version of claims, following the structure that claims will eventually take as JWts. 

#### Claim Structure

In accordance wtih LifeID, Claims will be structured as follows. This represents the form of a decoded JWT. 


```
{
  "iat": 1534892939,
  "aud": "did:life:112342bb3bdcc55d1b89d14d0110df59b68c6d78f5d7ec9179",
  "exp": 1957463421,
  "name": "lifeID Verified Email Claim",
  "data": {
    "id": “https://lifeid.io/v1/claims/75434365",
    "type": [
      "Credential",
      "EmailCredential"
    ],
    "issuer": "https://lifeid.io/",
    "issued": "2018-08-21T16:08:59Z",
    "claim": {
      "email": "jon@lifeid.io"
    },
    "revocation": {
      "key": "0x8c98CcA1eff23CBDC6DD465e6240D5C3E0D61C06",
      "type": "Secp256k1"
    }
  },
  "iss": "did:life:112342bb3bdcc55d1b89d14d0110df59b68c6d78f5d7ec9179"
}
```


## Claims in the Creative Passport Beta

In the beta release of the Creative Passport, after collecting user feedback, claim creation and management will be imtegrated with the LifeId system. Please see [this diagram](https://whimsical.co/6rkP2iGsKU6NctMSsgPxRG) of how personas and DIDs will work in the beta.

### Services in the Creative Passport Beta

In the Creative Passport Alpha, there was a single service (the Creative Passport itself). We would like to begin opening up the ecosystem when we get into the Creative Passport Beta, but this should happen only after LifeID is able to provide us with an SDK of their API. This is due to key management issues for services (see "Server in the Middle", below.)


### Integration with LifeId

#### LifeId Integration
In the early stages of the Creative Passport beta, we will interact with LifeID via calls to their API bridge. Following solidification of their services (see "Server-in-the-Middle"), we will integrate our backend with their SDK, which will provide API interaction. 

#### Server-in-the-middle
In the early stages of the Creative Passport beta, LifeId will have a server between their API-bridge server and the Creative Passport API backend that acts as a private key store. This server is able to do signing and send requests to the LifeId API on our behalf. This is a temporary fix that allows LifeID to integrate with us while still solidifying their API and shfiting their implementation from Ethereum to Rchain. We will integrate with LifeId's OIDC server in order to have access to this system.

When implementations have solidfied, LifeID will move their API into an SDK that we run on our servers, and we will resume management of all keys. 

#### LifeID Specifications

##### JWTs
LifeId's system is powered via the exchange of JWTs (containing claim data) throughout their system. We will work in accordance with LifeId's JWT specifications, which are identical to the standards being proposed by the W3C. 

##### Blockchain interaction
As currently designed, it is possible for us to integrate with LifeId's services in a couple of ways. 
1. We could issue all claims within our system, and only send revocations through the LifeID API Bridge to be written to the chain. Verifications also require a call to the LifeID API bridge to ensure that the claim has not been revoked (or a read of the chain via other mechanics). In this scenario, personas would not be written to the chain.

2. In addition to the functionality described in (1), we could register personas on the chain in order to allow users to take advantage of LifeId's key backup functionality.  

There are expenses in both scenarios, with greater expense being incurred via Scenario 2. Testing of the Alpha will allow us to begin to model what these expenses will look like. 

The hope for the Creative Passport has been that, in a future release, users could login with their existing LifeID app, or with their uport app, or with any other distributed identifier provider, in addition to being able to create a root identity via the Creative Passport system. It's still unclear how this would work if we go for backup integration with LifeID via scenario 2. 

If we do not pick scenario 2, we will need to implement some other key back-up mechanism in order to ensure that our users do not suffer from catastrophic data loss, given that many of our users will be inexperienced wtih key management. 

#### Life ID Current API documentation

**lifeID Claim Verification/Issuer Endpoints**

`POST /claims/:did/request/:type   body: {signedJWT: string}`

`POST /claims/:did/verify body: {signedJWT: string`


**lifeID Org API Endpoints**
* n.b. we wil not implmeent Org dids in initial releases of the passport *

```
POST /api/users  body {

  "did": "string",

  "publicKey": "string",

  "signedHash": "string",

  "recoveryKey": "string"

}  — create user

```

`POST /api/did/sign     body{ dataToSign:string }     — sign data with did`

`POST /api/did/verify  body {signedJWT: string}      — verify data with did`

`POST /api/recover   body {signedPublicKey: string}   —recover`

`POST /api/claims/revoke body {signedClaimHash: string}`


**MobileSDK**

`generateUser() => {did, userRequest :{did, publicKey ,publicRecoveryKey,  signedHash}, keyPair: {private, public, address}, recoveryKeyPair: {private, public, address} }`

`generateDID() => {did, keyPair: {private, public, address}, recoveryKeyPair: {private, public, address} }`

`generateLifeIDClaimRequest(didPublicPrivate, type, claimData) => signedJWT`

`generateLifeIDVerifyClaimRequest(didPublicPrivate,  verificationCode) => signedJWT`


### Structure of the Creative Passport Beta Claim Services

In the beta of the Crreative Passport, claim services will be abstracted into two modules
- Issuance Module
- Verification Module
- Revocation Module

#### Issuance Module
The issuance module will take care of processing requests for claim creation, from both personas and from service_dids. 

In version 1 of the beta, in which LifeID holds our keys in the "Server in the Middle" scenario, we will request signing of sevice claims via an API call. 


#### Verification Module
This module will check signatures on claims and check for claim revocations via the LifeID API Bridge/integration wtih the LifeID SDK.

If calling the LifeID service for claim verificiation is too expensive, we could look into ways to search for revoked claims via chain inspection. This is an open question and needs to be explored in further depth. 

### Revocation Module

This module will revoke claims via calls to the LifeID service. 


### Transition from the Creative Passport Alpha to Beta

Users will be notified of our plans to launch a beta version of the existing service. They will be given the option of "starting from scratch" on the beta, or of carrying their existing social graph claims into the beta. Should they accept the latter, when the beta launches, the claims carried in the "claims" table of the alpha will be mass-issued by the Claim Issuance Module, with a user approving/signing the claims either en masse or one by one. 




