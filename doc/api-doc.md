FORMAT: 1A

# Mycelia Creative Passport MVP API

The Creative Passport API accesses users, personas and their data, services, service dids and their data, and related claims. All possible requests, as well as expected responses, are detailed below, organised by object collection. 


## Brief introductory notes

### Order of operations

In order to run through the functionality of the Creative Passport MVP, you must create objects in the following order: 

- Create Service
- Create Service DID
- Create CreativePassport
- Create Persona

Dids -- Decentralised Identifiers -- represent in this version of the Creative Passport what will eventually be decentralised pub/private key pairs that are able to revoke and issue claims. In addition to having a DID associated with a root "creative_passport" or "service" account, users will initiate new DIDs with the creation of persona. 

Serivce and persona dids can have a data objects associated with them. This data object, PersonaData or ServiceData, represents data that will be stored in decentralised storage in the future. It is granularly permissioned and includes personal/contact information. 

After creating root services and dids, users can create persona_data and service_data in relation to each persona and service_did. 


### Social Graph

Social Graph connections are made between two Personas in reference to the Service with which they are associated. In the future, these connections will be made and revoked solely through the issuance and revocation of claims (see below).


### Claims

*For more information about Claims, in the alpha and in the beta, please see `claims.apib`* 

Claims represent creative_passport- and service-authorised connections between: service_did/persona, persona/persona/service, and persona/persona_data. In the future, claims will expand to also reprsent connections between creative_passport/persona. Each claim is issued by a persona or a service_did via signing with its public/private key pair. The service that is referenced by the claim or that issues the claim will check signatures before processing calls, thereby determining the flow and display of data throughout the Creative Passport network. Claims can be revoked at any time by the isusing entity. 

At the moment, Claims are created passively: upon social graph connection initiation, upon DID creation, and upon persona_data creation. At the moment, claims are merely pseduo-connections (collected in the claims table) that represent these connections between service/did, did/did/service, and did/data. The "claim" field, where the hstore of claim data goes, is currently empty and needs to be implemented. When the Creative Passport is decentralised, these claim object will be transformed en masse into proper claims. 



## Requests and Responses


## CreativePassports Collection [/twirp/mycelia.api.creativepassport.CreativePassportService/]


### Create a New CreativePassport [POST /twirp/mycelia.api.creativepassport.CreativePassportService/CreateCreativePassport]

Used for signup.

+ Request (application/json)

        {
            "email": "john@doe.com",
            "username": "johnd",
            "password": "password"     
          }

+ Response 201 (application/json)

    + Body

            {
                "id": "b09cd642-9df8-4f44-bb5a-169829043110",
                "email": "john@doe.com",
                "admin": "false", 
            }



#### Note that after creating a creativePassport, you must login with the creativePassport in order to access the JWT token

### Login CreativePassport [POST /twirp/mycelia.api.creativepassport.CreativePassportService/Login]

+ Request (application/json)

{
  "email": "email@email.com",
  "password": "password",
}


+ Response 201 (application/json)

{
  "token" : "jwt.token.returned"
}


#### Please note that all subsequent requests MUST include the token in the authorisation header, as:

curl --request POST --header "Content-Type: application/json, Authorization: Bearer <token>"




### Get CreativePassport [POST /twirp/mycelia.api.creativepassport.CreativePassportService/GetCreativePassport]

+ Request (application/json)

{"id":"0ce68203-a61e-4fc6-bc38-7e2839aa7b60"}'


+ Response 200 (application/json)

    {
      "id":"0ce68203-a61e-4fc6-bc38-7e2839aa7b60",
      "username":"lutherblisset",
      "email":"lblisset@blisset.com", 
      "associated_dids":[],
    }






### Update a CreativePassport [POST /twirp/mycelia.api.creativepassport.CreativePassportService/UpdateCreativePassport]

# Note that at the moment, all required fields need to be passed to the update statement. We should consider switching query libraries in order to not have to update all fields ? 
# n.b. You can't update admin over request (security until we have more robust privilege infrastructure in place)

+ Request (application/json)


   '{
      "id":"41edc2f4-e1c3-4ef9-8cf3-79f04b514361", 
      "username": "luther", 
      "email":"blisset@blisset.com"
    }'

+ Response 204 (application/json)



### Delete a CreativePassport [POST /twirp/mycelia.api.creativepassport.CreativePassportService/DeleteCreativePassport]
Deleting a creativePassport automatically deletes:
  - All associated Personas
  - All associated PersonaData
  - All associated addresses
And revokes
  - All associated claims

+ Request (application/json)

   {"id":"6288e2a1-ae1e-44a6-814b-af66fb1ae0b3"}


+ Response 204 (application/json)



## Persona Collection [/twirp/mycelia.api.creativepassport.PersonaService/]

### Create a New Persona [POST /twirp/mycelia.api.creativepassport.PersonaService/CreatePersona]

Personas are associated with root creative_passports. Every root creative_passport can have multiple personas, and generally does. In the future, each persona will represent a unique persona in the Createive Passport (e.g. producer, jazz saxaphonist, & c.)

To add a claim or persona_data, refer to dedicated endpoint.

+ Request (application/json)

        {
            "did": "sss",
            "private_key": "sss",
            "public_key": "sss",
            "ethereum_address": "ssss",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "8d76b2d6-63c6-4867-be9d-aef0c99a005c",
            "service_did_id": "6bb2f326-9c1f-46bd-a239-b1a6887c79f5",
            "did_type": "user"
        }



+ Response 201 (application/json)

    + Body
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "user"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []

        }



### Get Persona [POST http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/GetPersona]


+ Request (application/json)

   {"id":"b09cd642-9df8-4f44-bb5a-169829043110"}


+ Response 201 (application/json)

        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "user"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }


### Update Persona [POST http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/UpdatePersona]

If the request body doesn't contain a certain persona attribute, it will be set to its zero value.

service_did_id and creative_passport_id are not updatable.

To add or update associated_data, cf. dedicated endpoints Add/UpdatePersonaData and Add/Update Claims
Updating Persona automatically revokes and creates new claims. To update claims  more granularly, see specific claim endpoints, e.g. UpdateClaim, DeleteClaim, and CreateClaim

+ Request (application/json)

        {
            "id": "62a497bc-db1a-43ab-b9c5-0b021fa7abef",
            "did": "dddd",
            "private_key": "dddd",
            "public_key": "dddd",
            "ethereum_address": "ddd",
            "notes": "any admin notes",
            "active": false,
            "privacy": false,
            "did_type": "persona"
        }


+ Response 204 (application/json)

### Delete Persona [POST /twirp/mycelia.api.creativepassport.PersonaService/DeletePersona]
Deleting a Persona automatically deletse all associated PersonaData and all associated Addresses
It revokes all associated claims 

+ Request (application/json)

        {
            "id": "b09cd642-9df8-4f44-bb5a-169829043110"
        }

+ Response 204 (application/json)

## PersonaData Collection [/twirp/mycelia.api.creativepassport.PersonaDataService/] 


### Create PersonaData Associated with Persona [POST /twirp/mycelia.api.creativepassport.PersonaDataService/CreatePersonaData]

+ Request (application/json)

  {
    "persona_id":"bd498b3b-be2d-4678-9c18-a587467d8a6d", 
    "biography": "four score and seven eyars", 
    "full_name":"Andy Something", 
    "first_name":"handle this different", 
    "last_name":"Change the field", 
    "avatar": "blob", 
    "biography_priv": true, 
    "address": {
                "data": {"some": "address"}
              }
    }


+ Response 201 (application/json)

    + Body
    {
      "full_name":"Andy Something",
      "first_name":"handle this different",
      "last_name":"Change the field",
      "biography":"four score and seven eyars",
      "address":
        {
          "id":"a3412908-21a3-4a44-84a6-dec305691773",
          "data":
            {
              "some":"address"
              }
        },
        "persona_id":"b1a00eab-d066-48b2-8c06-d3d158344202",
        "avatar":"blob"
      }

### Get "Persona Data" [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaDataService/GetPersonaData"]

+ Request
      {"id":"54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba"}

+ Response 201 (application/json)

    {
      id":"54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba",
      "full_name":"Andy Something",
      "first_name":"handle this different",
      "last_name":"Change the field",
      "biography":"four score and seven eyars",
      "address":
        {"id":"a3412908-21a3-4a44-84a6-dec305691773",
        "data":
          {
            "some":"address"
          }
        },
        "persona_id": "62a497bc-db1a-43ab-b9c5-0b021fa7abef",
        "avatar":"blob",
        "associated_claims":
          {
            "id":"62a497bc-db1a-43ab-b9c5-0b021fa7abef",
            "claim":"hstore",
            "issuing_persona_id": "2a497bc-db1a-43ab-b9c5-0b021fa7abef",
            "subject_persona_id":"",
            "issuing_service_id":"2a497bc-db1a-43ab-b9c5-0b021fa7abef",
            "subject_service_id":"",
            "associated_service_did_id":"",
            "data_id": "2a497bc-db1a-43ab-b9c5-0b021fa7abef",
            "claim_type": "data",
            "social_graph_priv": "false",
            "valid":"true"

          }
    }


### Update "Persona Data" [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaDataService/UpdatePersonaData"]

n.b. You need to pass all updateable fields with the request or they will be overwritten (as with UpdateCreativePassport and UpdatePersona). personaId is not updateable. Updating PersonaData automatically revokes and creates a new Claim. 



+ Request
  {
    "id":"b52fca17-f5f8-42b8-b856-4e48adbeaaa2",
    "persona_id":"bae00fb7-beaf-4823-b381-410cc45f153f", 
    "biography": "something else!",
     "full_name":"Andy Something", 
     "first_name":"handle this different", 
     "last_name":"phew the field", 
     "avatar": "blob", 
     "biography_priv": true, 
     "address":
        {
          "id":"a955e19a-394f-4d5b-99f7-93e18e2017ea",
          "data":
            {
              "didfferent":"address"
              }
            }
          }

+ Response 204 (application/json)


### Delete "Persona Data"

n.b. Deleting PersonaData deletes all associated addresses and deletes all associated claims


+ Request
  {"id":"89e82132-e479-4da0-a255-e24476ef74de"}

+ Response 204 (application/json)


## Service Collection [/twirp/mycelia.api.creativepassport.ServiceService/]

### TODO : only "admin" can create and edit services


###Create Service [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServicService/CreateService]

+ Request
    {
        "username": "Mycelia",
         "email": "email@mycelia.org"
    }


+ Response 201 (application/json)


    + Body

            {
                "id": "b09cd642-9df8-4f44-bb5a-169829043110",
                "email": "email@mycelia.org",
                "username": "Mycelia"
            }


### Get Service [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceService/GetService]

+ Request

  {"id":"b09cd642-9df8-4f44-bb5a-169829043110"}'


+ Response 201 (application/json)

    {
      "id":"b09cd642-9df8-4f44-bb5a-169829043110",
      "username":"Mycelia",
      "email":"email@mycelia.org", 
      "associated_dids":[],
    }



### Update Service [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceService/UpdateService]

# Note that at the moment, all required fields need to be passed to the update statement. We should consider switching query libraries in order to not have to update all fields ? 

+ Request (application/json)


   '{
      "id":"b09cd642-9df8-4f44-bb5a-169829043110", 
      "username": "Imogen", 
      "email":"email@mycelia.org"
    }'


+ Response 204 (application/json)


### Delete Service [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceService/DeleteService]

+ Request

  {"id":"b09cd642-9df8-4f44-bb5a-169829043110"}'


+ Response 204 (application/json)



## ServiceDid Collection [/twirp/mycelia.api.creativepassport.ServiceDidService/]


### Create ServiceDid [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDidService/CreateServiceDid]
  + Request 
        {
            "did": "ssss",
            "private_key": "ddd",
            "public_key": "ddddd",
            "ethereum_address": "ffff",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "service_id": "6777d244-a409-428b-a841-9bdac8d5a0a3",
            "did_type": "service"
        }

  + Response 201 (application/json)

          {
            "id": "b09cd642-9df8-4f44-bb5a-169829043110"
            "did": "ssss",
            "private_key": "ddd",
            "public_key": "ddddd",
            "ethereum_address": "ffff",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "service_id": "f0088c6a-7086-46f1-bc61-f4647f102247",
            "did_type": "service",
            "associated_data": [],
            "issued_claims": [],
            "subject_claims": [],
            "associated_claims": []
        }



### Get ServiceDid [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDidService/GetServiceDid]

+ Request (application/json)

   {"id":"b09cd642-9df8-4f44-bb5a-169829043110"}


+ Response 201 (application/json)

        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "service_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "service",
            "issued_claims" : [],
            "subject_claims": [],
            "associated_claims": [],
            "associated_data": []
        }



### Update ServiceDid [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDidService/UpdateServiceDid]

If the request body doesn't contain a certain persona attribute, it will be set to its zero value.

service_id is not updatable.

To add or update associated_data, cf. dedicated endpoints Add/UpdateServiceData and Add/Update Claims
Updating ServiceDid automatically revokes and creates new claims. To update claims  more granularly, see specific claim endpoints, e.g. IssueServiceClaim, UpdateServiceClaim, DeleteServiceClaim

+ Request (application/json)

        {
            "id": "62a497bc-db1a-43ab-b9c5-0b021fa7abef",
            "did": "NEWDID",
            "private_key": "NEWKEY",
            "public_key": "NEWKEYPUB",
            "ethereum_address": "NEWADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "did_type": "service"
        }


+ Response 204 (application/json)

### Delete Service Did [POST /twirp/mycelia.api.creativepassport.PersonaService/DeleteServiceDid]
Deleting a ServiceDid automatically deletse all associated ServiceData and all associated Addresses
It revokes all associated claims 

+ Request (application/json)

        {
            "id": "b09cd642-9df8-4f44-bb5a-169829043110"
        }

+ Response 204 (application/json)



## ServiceData Collection [/twirp/mycelia.api.creativepassport.ServiceDataService/] 


### Create ServiceData associated with ServiceDid [POST http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDataService/CreateServiceData]

+ Request (application/json)

  {
    "service_did_id":"637021f8-1026-45af-8dd9-148a52a90906", 
    "biography": "four score and seven eyars", 
    "full_name":"Andy Something", 
    "first_name":"handle this different", 
    "last_name":"Change the field", 
    "avatar": "blob", 
    "biography_priv": true, 
    "address": {
                "data": {"some": "address"}
              }
  }


+ Response 201 (application/json)

    + Body
    {
      "id":"54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba",
      "full_name":"Andy Something",
      "first_name":"handle this different",
      "last_name":"Change the field",
      "biography":"four score and seven eyars",
      "address":
        {
          "id":"a3412908-21a3-4a44-84a6-dec305691773",
          "data":
            {
              "some":"address"
              }
        },
        "service_did_id":"78aece37-4569-45c6-84f9-e9e8069d2f5c",
        "avatar":"blob"
        "associated_claims": []
      }

### Get "Service Data" [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDataService/GetServiceData"]

+ Request
      {"id":"54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba"}

+ Response 201 (application/json)

    {
      id":"54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba",
      "full_name":"Andy Something",
      "first_name":"handle this different",
      "last_name":"Change the field",
      "biography":"four score and seven eyars",
      "address":
        {"id":"a3412908-21a3-4a44-84a6-dec305691773",
        "data":
          {
            "some":"address"
          }
        },
        "service_did_id": "62a497bc-db1a-43ab-b9c5-0b021fa7abef",
        "avatar":"blob",
        "associated_claims": []
    }


### Update "Service Data" [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDataService/UpdateServiceData"]

n.b. You need to pass all updateable fields with the request or they will be overwritten (as with UpdateCreativePassport and UpdatePersona). personaId is not updateable. Updating Service Data automatically revokes and reissues a DataClaim. 

+ Request
  {
    "id":"89e82132-e479-4da0-a255-e24476ef74de",
    "service_did_id":"b1a00eab-d066-48b2-8c06-d3d158344202", 
    "biography": "something else!",
     "full_name":"Andy Something", 
     "first_name":"handle this different", 
     "last_name":"phew the field", 
     "avatar": "blob", 
     "biography_priv": true, 
     "address":
        {
          "id":"28eac35b-d244-4504-84a0-4d9ced575935",
          "data":
            {
              "didfferent":"address"
              }
            }
  }

+ Response 204 (application/json)


### Delete "Service Data"

n.b. Deleting ServiceData deletes all associated addresses and revokes all associated claims

+ Request
  {"id":"89e82132-e479-4da0-a255-e24476ef74de"}

+ Response 204 (application/json)



## Social Graph Collection [/twirp/mycelia.api.creativepassport.PersonaService/]
(This is an extension of Persona collection for now)

#### Please note that you should use these functions to retrieve all social graph information (as opposed to "GetPersona" and "GetPersonaData"). This is because we want to keep the retrieval functions separate in order to mimic what these calls will be like when we decentralise. If this becomes too confusing, please submit a complaint to your friendly golang programmer. 


### Create Social Graph Connection [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/CreateSocialGraphConnection"]

This creates a claim connecting two personas, with an associated service did written into the claim. Calling this function, for now, calls claim creation bi-directionally, e.g. one claim is created for each creative passport. 

+ Request
  {
      "req_social_graph":
        [
          {"id": "970f7f6b-daa1-4a25-af72-7ccc3be9652e"}
        ],
      "subj_social_graph": 
      [

      {"id": "b7ddf6d8-73b9-4156-b742-f2f5b651cdd0"}

      ],
      "social_graph_priv": "true"
}



+ Response 201 (application/json)

    {
      "req_social_graph":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

      "subj_social_grpah":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

      "social_graph_claims": 
      [


        {
          "id": "",
          "claim_type": "social",
          "claim": "",
          "issuing_persona_id": "",
          "subject_persona_id": "",
          "associated_service_did_id": "",
          "valid": "true",
          "social_graph_priv": "true"

        },


        {
          "id": "",
          "claim_type": "social",
          "claim": "",
          "issuing_persona_id": "",
          "subject_persona_id": "",
          "associated_service_did_id": "",
          "valid": "true",
          "social_graph_priv": "true"

        }


      ]

      "social_graph_priv": true
    }



### Get Social Graph Connection [GET "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/GetSocialGraphConnection"]

This function retrieves a connection when given two Personas. 
N.b. this response will return empty if (a) both social graph claims are not valid or (b) either persona is inactive 

  + Request
    {
        "req_social_graph":
          [
            {"id": " 66cd1887-5306-4305-b1a3-e70df0782eac"}
          ],
        "subj_social_graph": 
        [

        {"id":"ddc81fe1-939a-4fc4-befd-592707c17a86"}

        ]  
    }



+ Response 201 (application/json)

    {
      "req_social_graph":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

      "subj_social_grpah":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

      "social_graph_claims": 
      [


        {
          "id": "",
          "claim_type": "social",
          "claim": "",
          "issuing_persona_id": "",
          "subject_persona_id": "",
          "associated_service_did_id": "",
          "valid": "true",
          "social_graph_priv": "true"

        },


        {
          "id": "",
          "claim_type": "social",
          "claim": "",
          "issuing_persona_id": "",
          "subject_persona_id": "",
          "associated_service_did_id": "",
          "valid": "true",
          "social_graph_priv": "true"

        }


      ]

      "social_graph_priv": true
    }

### Update Social Graph Connection [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/UpdateSocialGraphConnection"]

This function "udpates" a social graph connection. It is largely used to change privacy settings on the social graph connection, which, for the MVP, are probably defunct. Privacy settings on a social graph connection will allow users to regulate the publicly viewable status of their connection. Calling this function "revokes" exisitng claims between the users and reissues them. 

  + Request


  {
      "req_social_graph":
        [
          {"id": "54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba"}
        ],
      "subj_social_graph": 
      [

      {"id":"a3412908-21a3-4a44-84a6-dec305691773"}

      ],
      "social_graph_priv": "false"
}


+ Response 204 (application/json)



### Delete Social Graph Connection [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/DeleteSocialGraphConnection"]

This function will delete a social graph link between two users. It also deletes the associated claims. 


  + Request


  {
      "req_social_graph":
        [
          {"id": "54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba"}
        ],
      "subj_social_graph": 
      [

      {"id":"a3412908-21a3-4a44-84a6-dec305691773"}

      ],
      "social_graph_priv": "false"
}


+ Response 204 (application/json)



### Get Persona Social Graph [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/GetSocialGraphConnections"]

Whem supplied with a persna's DID, this function returns all active members of the persona's social graph
(N.B. The DID currently serves as auth, this should be changed)
TODO : This should probably return a list of DIDs instead of the social graph connection response for some arbitraty reason? 


  + Request
    { "id": "54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba"}



+ Response 201 (application/json)



    [
      {
      "req_social_graph":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

      "subj_social_grpah":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

  

      "social_graph_priv": true
    },


        {
      "req_social_graph":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

      "subj_social_grpah":
      [
        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }

      ],

      "social_graph_priv": true
    }

  ]


### Get Public Social Graph [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.PersonaService/GetPublicSocialGraph"]

When supplied with a Persona's Did, this function returns all members of the public graph 
(N.B. the DID currentl serves as auth, this should be changed)

  + Request
    { "id": "54b8a3cd-f8b1-411b-a06f-bbc5c483d1ba"}


  + Response 201 (application/json)

  [
    {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        },



        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        },



        {
            "id": ""b09cd642-9df8-4f44-bb5a-169829043110",
            "did": "DIDSTRINGHERE",
            "private_key": "THISISGENERATEDONFRONTEND",
            "public_key": "ALSOGENERATEDONFRONTEND",
            "ethereum_address": "ASSOCIATEDTESTNETADDRESS",
            "notes": "any admin notes",
            "active": true,
            "privacy": true,
            "creative_passport_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "service_did_id": "b09cd642-9df8-4f44-bb5a-169829043110",
            "did_type": "persona"
            "issued_claims" : [],
            "subject_claims": [],
            "associated_data": []
        }


  ]



## Claims Suite [/twirp/mycelia.api.creativepassport.ServiceDidService/]

Claim retrieval is currently tied to the Service Did suite. Witth these functions, you can create, update, get, and delete claims independently of the other functions on the site. Please do this at your own risk! The API is designed to create all necessary claims as part of the data/persona/social graph creation process, and creating/updating/deleteing them separately might result in unintended consequences (e.g. personas not returning correctly, etc.). Ostensibly, these endpoints are for SERVICES, but they will work for any claim in the system. For most functions, you will need to know the claim ID. We need to play around to see how much need there is for more controlled endpoints of this sort. 



### Issue Claim [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDidService/IssueServiceClaim"]


+ Request (application/json)


        {
          "claim_type": "service",
          "claim": "",
          "issuing_persona_id": "",
          "subject_persona_id": "",
          "issuing_service_id": "",
          "subject_service_id": "",
          "associated_service_did_id": "",
          "data_id": "",
          "valid": "true",
          "social_graph_priv": "true"

        }


  + Response 201 (application/json)

      {
         "id": "",
         "claim_type": "service",
          "claim": "",
          "issuing_persona_id": "",
          "subject_persona_id": "",
          "issuing_service_id": "",
          "subject_service_id": "",
          "associated_service_did_id": "",
          "data_id": "",
          "valid": "true",
          "social_graph_priv": "true"

        }



### Update Claim [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDidService/UpdateServiceClaim"]

+ Request (application/json)


        {
          "id": "",
          "claim_type": "service",
          "claim": "",
          "issuing_persona_id": "",
          "subject_persona_id": "",
          "issuing_service_id": "",
          "subject_service_id": "",
          "associated_service_did_id": "",
          "data_id": "",
          "valid": "true",
          "social_graph_priv": "true"

        }


  + Response 204 (application/json)


### Delete Claim [POST "http://localhost:8080/twirp/mycelia.api.creativepassport.ServiceDidService/DeleteServiceClaim"]

+ Request (application/json)

  {"id": ""}


+ Response 204 (application/json)


TODO distinct functions to return all claims for service, for user, etc. 
