package main

import (
	"fmt"
	"github.com/docker/distribution/context"
	"net/http"
	"github.com/rs/cors"
    "github.com/joho/godotenv"
    "os"
	"log"

	authServer "mycelia/internal/server/auth"
	creativePassportServer "mycelia/internal/server/creativepassport"
	personaServer "mycelia/internal/server/persona"
	personaDataServer "mycelia/internal/server/personadata"
	serviceServer "mycelia/internal/server/service"
	serviceDidServer "mycelia/internal/server/servicedid"
	serviceDataServer "mycelia/internal/server/servicedata"

	authRpc "mycelia/rpc/auth"
	creativePassportRpc "mycelia/rpc/creativepassport"
	personaRpc "mycelia/rpc/persona"
	personaDataRpc "mycelia/rpc/personadata"
	serviceRpc "mycelia/rpc/service"
	serviceDidRpc "mycelia/rpc/servicedid"
	serviceDataRpc "mycelia/rpc/servicedata"
	"mycelia/internal/hooks/auth"
	"mycelia/internal/database"
)

func WithURLQuery(base http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		query := r.URL.Query()
		ctx = context.WithValue(ctx, "query", query)
		r = r.WithContext(ctx)

		base.ServeHTTP(w, r)
	})
}

func main() {
	fmt.Printf("User Service on :8080")
	err := godotenv.Load()
	if err != nil {
	log.Fatal("Error loading .env file")
	}

	// determine testing v dev for database
	var db_connect bool
	if os.Getenv("TESTING") == "true" {
		db_connect = true
	}else{
		db_connect = false
	}

	db := database.Connect(db_connect)
  	
  	hooks := auth.JWTCheckerHooks()

  	newAuthServer := authServer.NewServer(db)
  	authTwirpHandler := WithURLQuery(authRpc.NewAuthServiceServer(newAuthServer, nil))

	newCreativePassportServer := creativePassportServer.NewServer(db)
	creativePassportTwirpHandler := WithURLQuery(creativePassportRpc.NewCreativePassportServiceServer(newCreativePassportServer, hooks))

	newPersonaServer := personaServer.NewServer(db)
	personaTwirpHandler := WithURLQuery(personaRpc.NewPersonaServiceServer(newPersonaServer, hooks))

	newPersonaDataServer := personaDataServer.NewServer(db)
	personaDataTwirpHandler := personaDataRpc.NewPersonaDataServiceServer(newPersonaDataServer, hooks)

	newServiceServer := serviceServer.NewServer(db)
	serviceTwirpHandler := WithURLQuery(serviceRpc.NewServiceServiceServer(newServiceServer, hooks))

	newServiceDidServer := serviceDidServer.NewServer(db)
	serviceDidTwirpHandler := WithURLQuery(serviceDidRpc.NewServiceDidServiceServer(newServiceDidServer, hooks))

	newServiceDataServer := serviceDataServer.NewServer(db)
	serviceDataTwirpHandler := WithURLQuery(serviceDataRpc.NewServiceDataServiceServer(newServiceDataServer, hooks))

	mux := http.NewServeMux()
	mux.Handle(authRpc.AuthServicePathPrefix, authTwirpHandler)
	mux.Handle(creativePassportRpc.CreativePassportServicePathPrefix, creativePassportTwirpHandler)
	mux.Handle(personaRpc.PersonaServicePathPrefix, personaTwirpHandler)
	mux.Handle(personaDataRpc.PersonaDataServicePathPrefix, personaDataTwirpHandler)
	mux.Handle(serviceRpc.ServiceServicePathPrefix, serviceTwirpHandler)
	mux.Handle(serviceDidRpc.ServiceDidServicePathPrefix, serviceDidTwirpHandler)
	mux.Handle(serviceDataRpc.ServiceDataServicePathPrefix, serviceDataTwirpHandler)

	// cors.Default() setup the middleware with default options being
	// all origins accepted with simple methods (GET, POST).
	handler := cors.Default().Handler(mux)
	handler = auth.WithJWT(handler)
	
	http.ListenAndServe(":" + os.Getenv("SERVER_PORT"), handler)
	defer db.Close()
}
