package main

import (
    "context"
    "net/http"
    "os"
    "fmt"

    pb "mycelia/rpc/creativepassport"
)

//This file exists only for testing purposes at the moment. 

func main() {
    client := pb.NewUserServiceProtobufClient("http://localhost:8080", &http.Client{})

    u1, err := client.CreativeCreativePassport(context.Background(), &pb.CreativePassport{Username: "janed", Email: "jane@d.com"})
    // u2, err := client.CreateUser(context.Background(), &pb.User{Name: "marie", Email: "marie@doe.com"})
    // users, err := client.GetUsers(context.Background(), &pb.Empty{})
    if err != nil {
        fmt.Printf("oh no: %v", err)
        os.Exit(1)
    }
    fmt.Printf("New creativePassport created: %+v\n", u1)
    // fmt.Printf("New user created: %+v\n", u2)
    // fmt.Printf("Users: %+v\n", users)
}
